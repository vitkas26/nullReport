package io.onteam.onoi.objects

/**
 * Created by sedi_user on 30.03.2018.
 */
class GetUserResponse {
    var message: String = ""
    var profile: UserProfile? = null
}