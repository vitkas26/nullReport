package io.onteam.onoi.objects

class ArchiveGroup(val name: String, val reports: Array<ArchiveReport> = emptyArray())