package io.onteam.onoi.objects

class News(val title: String, val content: String, val date: String, val main_img: String?,
           val source: String, val source_url: String )