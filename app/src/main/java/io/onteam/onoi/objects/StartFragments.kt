package io.onteam.onoi.objects

enum class StartFragments {
    LangChooseFragment,
    ChooseActionFragment
}