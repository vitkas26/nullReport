package io.onteam.onoi.objects

enum class Subjects(val type: Int) {
    MEDIUM(0),
    LARGE(1)
}