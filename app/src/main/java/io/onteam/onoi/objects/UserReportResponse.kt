package io.onteam.onoi.objects


class UserReportResponse {
    var success: Boolean = false
    var message: String = ""
    var forms: Array<ReportGroup> = emptyArray()
}