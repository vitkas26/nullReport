package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_wiki.*
import kotlinx.android.synthetic.main.fragment_wiki.view.*

class WikiFragment : Fragment(), FragmentVisibleListener {

    private val wikiUrl = "http://curator.onoi.kg/api.php/get-wiki"
    private val layoutRes = R.layout.fragment_wiki
    private var isVisibleNow = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        initWebView(view)
        return view
    }

    private fun initWebView(view: View?) {
        view?.apply {
            wv_wiki.apply {
                webViewClient = CustomWebViewClient()
                webChromeClient = CustomWebChromeClient()
                settings.javaScriptEnabled = true
                loadUrl(wikiUrl)
            }
        }
    }

    override fun setVisible() {
        isVisibleNow = true
        (activity as? MainActivity)?.updateFab(null)
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }

    fun canGoBack() = wv_wiki.canGoBack()

    fun goBack() = wv_wiki.goBack()

    inner class CustomWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            view?.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, url)
        }
    }

    inner class CustomWebChromeClient : WebChromeClient()


}
