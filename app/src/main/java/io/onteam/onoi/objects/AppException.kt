package io.onteam.onoi.objects

object AppException {
    const val empty_response_message = "Empty server response"
    const val not_success_response_message = "Server response is not success"
    const val unknown_message = "Unknown exception"
    const val failure_response_message = "Request failed"
}