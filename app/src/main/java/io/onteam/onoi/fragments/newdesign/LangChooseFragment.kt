package io.onteam.onoi.fragments.newdesign


import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.activities.IntroActivity
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.objects.OnoyConnector
import io.onteam.onoi.objects.SignInfo
import io.onteam.onoi.objects.UserManager
import io.onteam.onoi.objects.UserProfile
import io.onteam.onoi.utils.*
import kotlinx.android.synthetic.main.fragment_select_lang.*


class LangChooseFragment : BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_select_lang
    private var inn = ""
    private var keyword = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()

        NotificationHelper.clearNotification(id)

        val requireContext = requireContext()
        val packageInfo = requireContext.packageManager.getPackageInfo(requireContext.packageName, 0)
        tv_version.text = packageInfo.versionName

        tryStartLoad(view)
    }

    private fun tryStartLoad(view: View) {
        if (Helpers.checkConnection(requireContext()))
            startLoad(view)
        else {
            AlertDialog.Builder(requireContext(), R.style.ExitAlertDialogTheme)
                    .setTitle(R.string.error)
                    .setMessage(R.string.no_connect)
                    .setNegativeButton(R.string.close) { _, _ -> requireActivity().finish() }
                    .setPositiveButton(R.string.retry) { _, _ -> tryStartLoad(view)}
                    .create().show()
        }
    }

    private fun startLoad(view: View) {
        if (UserManager.isValidCurrentUser()) {
            startActivity(MainActivity.getIntent(requireContext()))
            requireActivity().finish()
        } else {
            inn = Preferences.getString(Preferences.Keys.USER_INN)
            keyword = Preferences.getString(Preferences.Keys.USER_KEYWORD)
            view.postDelayed({ handleAction() }, 3000)
        }
    }

    private fun handleAction() {
        when {
            isValidUserForRestore() -> {
                signIn()
            }
            else -> {
                val wrapper = ViewAnimateWrapper(cl_lang)
                val animDuration: Long = 500
                val animator = ObjectAnimator
                        .ofFloat(wrapper, "weight", wrapper.getWeight(), 1.0f)
                        .setDuration(animDuration)
                animator.start()

            }
        }
    }


    private fun signIn() {
        val firebaseKey = Preferences.getString(Preferences.Keys.FIREBASE_KEY)
        val signInfo = SignInfo(inn, keyword, firebaseKey)
        val dialog = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)
        val signInTask = OnoyConnector.signIn(signInfo) { exception, userResponse ->
            dialog.dismiss()
            if (exception != null) {
                if(exception.isNetworkException)
                    showConnectionErrorDialog()
                cl_lang.setVisible(true)
            }

            if (userResponse?.profile != null) {
                UserManager.setCurrentUser(userResponse.profile as UserProfile)
                startActivity(MainActivity.getIntent(requireContext()))
                requireActivity().finish()
            } else {
                cl_lang.setVisible(true)
            }
        }
        dialog.setOnCancelListener { signInTask.cancel() }
        dialog.show()
    }

    private fun showConnectionErrorDialog() {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setTitle(R.string.error)
                .setMessage(R.string.data_not_received_message)
                .setNegativeButton(R.string.close) { _, _ -> requireActivity().finish() }
                .setPositiveButton(R.string.retry) { _, _ -> signIn()}
                .create().show()
    }

    private fun isValidUserForRestore() = inn.isNotEmpty() && keyword.isNotEmpty()

    private fun initViews() {
        with(view) {
            btn_next.setOnClickListener { onNextBtnClick() }
        }
    }

    private fun onNextBtnClick() {
        saveChooseLang()
        startActivity(MainActivity.getIntent(requireContext()))
        showIntroIfNeeded()
        requireActivity().finish()
    }

    private fun saveChooseLang() {

    }

    private fun showIntroIfNeeded() {
        val isShown = Preferences.getBool(Preferences.Keys.SHOW_INTRO)
        if (!isShown) {
            startActivity(Intent(requireContext(), IntroActivity::class.java))
        }
    }
}
