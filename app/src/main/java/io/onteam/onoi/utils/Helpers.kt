package io.onteam.onoi.utils

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.support.v7.app.AlertDialog
import android.util.Base64
import android.view.View
import android.view.inputmethod.InputMethodManager
import io.onteam.onoi.R


/**
 * Created by RAM on 24.03.2018.
 */
object Helpers {
    fun checkConnection(context: Context): Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return manager.activeNetworkInfo != null
    }

    fun hideKeyboard(context: Context, targetView: View) {
        val manager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.hideSoftInputFromWindow(targetView.windowToken, 0)
    }

    fun base64ToBmp(b64String: String?): Bitmap? {
        if(b64String.isNullOrEmpty()) return null
        return try {
            val decode = Base64.decode(b64String, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(decode, 0, decode.size)
        } catch (e: Exception) {
            Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8)
        }
    }

    fun showInvitationMessage(context: Context) {
        AlertDialog.Builder(context, R.style.CustomAlertDialogTheme)
                .setTitle(R.string.confirm_action)
                .setMessage(R.string.invitation_description_message)
                .setPositiveButton(R.string.invitation) { _, _ ->
                    createInvitationAndShare(context)
                }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }

    private fun createInvitationAndShare(context: Context) {
        val intent = Intent(Intent.ACTION_SEND)
        val url = "https://play.google.com/store/apps/details?id=${context.packageName}"
        intent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.invitation_message, url))
        intent.type = "text/plain"
        context.startActivity(
                Intent.createChooser(intent, context.getString(R.string.select_application)))
    }

    fun resizeImage(img: Bitmap?,sizeCompPercent: Int = 50): Bitmap? {
        if (img != null) {
            val percent: Float = sizeCompPercent.toFloat() / 100
            val newWidth = (img.width - (img.width * percent)).toInt()
            val newHeight = (img.height - (img.height * percent)).toInt()
            return Bitmap.createScaledBitmap(img, newWidth, newHeight, false)
        }
        return img
    }

    fun showKeyboard(context: Context, view: View) {
        val manager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.toggleSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED, 0)
    }
}