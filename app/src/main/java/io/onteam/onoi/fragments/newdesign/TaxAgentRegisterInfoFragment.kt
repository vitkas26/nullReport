package io.onteam.onoi.fragments.newdesign


import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.base.IBaseFragmentActivity
import io.onteam.onoi.fragments.FinishFragment
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.fragment_tax_agent_register_info.*


class TaxAgentRegisterInfoFragment : BaseFragment() {

    private var progressDialog: SpotsDialog? = null

    override fun getLayoutRes(): Int = R.layout.fragment_tax_agent_register_info

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = SpotsDialog(context, R.style.CustomSpotsDialog)
        val newRegistrationUserInfo = UserManager.registeredUser

        requestUserInfo(newRegistrationUserInfo){
            updateViewsInfo(newRegistrationUserInfo)
        }
    }

    private fun requestUserInfo(user: RegisteredUser, callback: (RegisteredUser) -> Unit) {
        val userInfo = OnoyConnector.getUserInfo(user.innOrganisation) { onoyException, registeredUser ->

            progressDialog?.dismiss()

            onoyException?.let {
                handleExceptionUserInfo(it, user, callback)
            }

            registeredUser?.let {
                if (it.Status != UserStatuses.ok) {
                    handleInvalidStatus(it.Status)
                    return@getUserInfo
                }
                handleUserInfoRespose(it, user, callback)
            }
        }
        progressDialog?.setOnCancelListener { userInfo.cancel() }
        progressDialog?.show()
    }

    private fun handleInvalidStatus(status: Int) {
        val exMessageRes = when (status) {
            UserStatuses.notFound -> R.string.user_not_found
            UserStatuses.searchError -> R.string.searching_error_message
            else -> R.string.error
        }
        AlertDialog.Builder(requireContext())
                .setMessage(exMessageRes)
                .setPositiveButton(R.string.retry, null)
                .create()
                .show()
    }

    private fun handleUserInfoRespose(newUser: RegisteredUser, oldUser: RegisteredUser, callback: (RegisteredUser) -> Unit) {
        newUser.innEmployee = oldUser.innEmployee
        newUser.innOrganisation = oldUser.innOrganisation
        newUser.tax_agent = oldUser.tax_agent

        UserManager.registeredUser = newUser

        callback(UserManager.registeredUser)

    }

    private fun handleExceptionUserInfo(onoyException: OnoyException, user: RegisteredUser, callback: (RegisteredUser) -> Unit) {
        AlertDialog.Builder(requireContext())
                .setMessage(onoyException.message)
                .setPositiveButton(R.string.retry) { _, _ ->
                    requestUserInfo(user, callback)
                }
                .setNegativeButton(R.string.close) { _, _ ->
                    requireActivity().finish()
                }
                .create()
                .show()
    }

    private fun updateViewsInfo(userInfo: RegisteredUser) {
        civ_inn.setText(userInfo.innOrganisation)
        civ_naming.setText(userInfo.Name)

        btn_next.setOnClickListener {
            Helpers.hideKeyboard(requireContext(), civ_naming)
            if (saveInputValues()) {
                doRegistration(userInfo)
            }
        }

        btn_back.setOnClickListener {
            popFragment()
        }
    }

    /**
     * Производит регистрацию/авторизацию пользователя в системе.
     * Если авторизация успешна - открываем основной экран приложения.
     * @param user информация о пользователе.
     */
    private fun doRegistration(user: RegisteredUser) {
        val registrationCall = OnoyConnector.registration(user) { exception, response ->
            progressDialog?.dismiss()

            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
            }

            if (response != null) {
                if (response.success) {
                    signIn(user.innOrganisation, "0")
                } else {
                    showAlertMessage(R.string.error, response.message)
                }
            }
        }
        progressDialog?.setOnCancelListener { registrationCall.cancel() }
        progressDialog?.show()
    }

    /**
     * Получает информацию по авторизуемому пользователю.
     * @param inn ИНН пользователя.
     * @param keyword кодовое слово.
     */
    private fun signIn(inn: String, keyword: String) {
        val firebaseKey = Preferences.getString(Preferences.Keys.FIREBASE_KEY)
        val signInfo = SignInfo(inn, keyword, firebaseKey)
        OnoyConnector.signIn(signInfo) { exception, userResponse ->
            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
            }

            if (userResponse != null) {
                saveForSignIn(inn, keyword)
                UserManager.setCurrentUser(userResponse.profile as UserProfile)
                (requireActivity() as? IBaseFragmentActivity)?.replaceFragment(
                        FinishFragment(),
                        false
                )
            }
        }
    }

    private fun saveForSignIn(inn: String, keyword: String) {
        Preferences.setValue(Preferences.Keys.USER_INN, inn)
        Preferences.setValue(Preferences.Keys.USER_KEYWORD, keyword)
    }

    /**
     * Отображает некоторое сообщение как @{code AlertDialog} с текстом @{code message}.
     * @param message сообщение для отображения.
     */
    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    private fun saveInputValues(): Boolean {
        val userInfo = UserManager.registeredUser
        val regionCode = userInfo.getRegionCode()
        val regionId = UserManager.regions.firstOrNull { region -> region.codeGni == regionCode }?.id
        userInfo.region_id = regionId ?: 0
        return true
    }
}
