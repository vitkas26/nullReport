package io.onteam.onoi.objects

data class SignInfo(val inn: String, val keyword: String, val firebaseKey: String)