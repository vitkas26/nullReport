package io.onteam.onoi.objects

enum class AuthNexts {
    give_me_pin,
    give_me_personal_inn
}