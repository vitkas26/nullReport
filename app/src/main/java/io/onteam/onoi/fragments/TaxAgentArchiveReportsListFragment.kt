package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.OnReportListChangeListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.dialogs.EmailPeriodDialog
import io.onteam.onoi.objects.*
import io.onteam.onoi.ui.ArchiveReportsAdapter
import io.onteam.onoi.ui.ReportsAdapter
import io.onteam.onoi.ui.TextSizedToast
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.fragment_tax_archive_reports_list.*
import kotlinx.android.synthetic.main.fragment_tax_archive_reports_list.view.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import java.util.*


/**
 *  Fragment используемый для отображения списка отчетов находящихся в архиве.
 */
class TaxAgentArchiveReportsListFragment : Fragment(), OnReportListChangeListener, FragmentVisibleListener, ReportsAdapter.ExpandChangeListener, UserManager.UserManagerChangeListener {

    private val layoutRes = R.layout.fragment_tax_archive_reports_list

    private val reports = mutableListOf<ArchiveGroupedItem>()
    private val adapter = ArchiveReportsAdapter(reports, this)

    private var monitoringTimer = Timer()
    private var timerIsRunning = false

    private var isVisibleNow = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        initViews(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        UserManager.changeListeners.add(this)
        requestInfo()
    }

    override fun onDestroyView() {
        cancelMonitoringTimer()
        UserManager.changeListeners.remove(this)
        super.onDestroyView()
    }

    /**
     * Инициализация компонентов fragment'a.
     * @param view родительский View.
     */
    private fun initViews(view: View) {

        view.rv_reports.adapter = adapter
        view.rv_reports.layoutManager = LinearLayoutManager(context)

        UserManager.getCurrentUser()?.let {
            view.tv_company_name.text = it.name
        }

        view.refresh_layout.setColorSchemeResources(R.color.colorAccent)
        view.refresh_layout.setOnRefreshListener {
            val hasProcessedReport = reports.any {
                it.reportsGroup.any {
                    it.reports.any { it.report_status_id == null }
                }
            }
            if (hasProcessedReport)
                showProcessedToast()
            refresh_layout.isRefreshing = false
        }
    }

    private fun showProcessedToast() {
        TextSizedToast(context!!, 18f).show(R.string.archive_request_timeout_message,
                Toast.LENGTH_LONG)
    }

    private fun showEmailPeriodDialog(inn: String) {
        EmailPeriodDialog.getInstance(inn).show(fragmentManager, "")
    }

    override fun reloadData() {
        requestInfo()
    }

    override fun currentUserInfoChange() {
        requestInfo()
    }

    /**
     * Запускает таймер для мониторинга состояния отчетов.
     * Делаем опрос раз в 1 минуту
     */
    private fun runMonitoringTimer() {
        if (timerIsRunning) return
        monitoringTimer.schedule(object : TimerTask() {
            override fun run() {
                requestInfo()
            }
        }, 60 * 1000, 60 * 1000)
        timerIsRunning = true
    }

    override fun onViewExpandChange(companyName: String?) {
        if (companyName != null) {
            scrollViewToItem(companyName)
        }
        val children = UserManager.getChildrenByName(companyName)
        val action = getActionForFabClick(children)
        updateFabVisibility(action)
    }

    private fun getActionForFabClick(children: UserProfile?): Runnable? {
        return if (children == null) {
            null
        } else {
            Runnable {
                showEmailPeriodDialog(children.inn)
            }
        }
    }

    private fun scrollViewToItem(companyName: String) {
        val item = adapter.items.find { it.company == companyName }
        val index = adapter.items.indexOf(item)
        rv_reports.scrollToPosition(index)
    }

    /**
     * Запращивает список отчетов из архива.
     */
    private fun requestInfo() {
        val user = UserManager.getCurrentUser() ?: return

        launch(UI) {
            reports.clear()
            resetEmptyLayout()

            if (!Helpers.checkConnection(requireContext())) {
                setEmptyPreload(EmptyPreload.getNoNetwork())
                return@launch
            }

            refresh_layout.isRefreshing = true

            user.children?.forEach { child ->
                val childReportsGroup = getCustomerArchive(child.inn).await()
                if (childReportsGroup?.forms != null && childReportsGroup.forms.isNotEmpty()) {
                    childReportsGroup.forms.forEach {
                        it.reports.sortByDescending { report -> report.datetime }
                    }
                    reports.add(ArchiveGroupedItem(child.name, childReportsGroup.forms))
                }
            }
            refresh_layout.isRefreshing = false
            adapter.notifyDataSetChanged()

            if (reports.isEmpty())
                setEmptyPreload(EmptyPreload.getNoSended())
            else {
                resetEmptyLayout()

                val hasProcessedReport = reports.any {
                    it.reportsGroup.any {
                        it.reports.any { it.report_status_id == null }
                    }
                }

                if (hasProcessedReport)
                    runMonitoringTimer()
                else
                    cancelMonitoringTimer()
            }

            ab_title.setVisible(!reports.isEmpty())
            refresh_layout.setVisible(user.hasCustomer())

            if (isVisibleNow)
                restoreFabState()
        }
    }

    private fun setEmptyPreload(preload: EmptyListPreload) {
        el_message.setPreload(preload)
    }

    private fun resetEmptyLayout() {
        el_message.resetPreload()
    }

    private fun updateFabVisibility(runnable: Runnable?) {
        (activity as? MainActivity)?.updateFab(
                FabUpdateEvent(R.drawable.ic_mail_black_24dp, FabUpdateEvent.Type.SendArchive, runnable))
    }

    private fun getCustomerArchive(inn: String) =
            async(CommonPool) {
                val response = OnoyConnector.getAbonentArchiveSync(inn).execute()
                if (response != null && response.isSuccessful) response.body()
                else null
            }

    /**
     * Останавливает таймер мониторинга состояния отчетов.
     */
    private fun cancelMonitoringTimer() {
        if (!timerIsRunning) return
        monitoringTimer.cancel()
        timerIsRunning = false
    }

    override fun setVisible() {
        isVisibleNow = true
        restoreFabState()
    }

    private fun restoreFabState() {
        val companyName = (rv_reports.adapter as ArchiveReportsAdapter).lastCompanyName
        val children = UserManager.getChildrenByName(companyName)
        val action = getActionForFabClick(children)
        updateFabVisibility(action)
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }

}
