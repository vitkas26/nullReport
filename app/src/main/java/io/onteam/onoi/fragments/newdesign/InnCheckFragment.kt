package io.onteam.onoi.fragments.newdesign


import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.fragments.RegistrationInfoFragment
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Helpers
import kotlinx.android.synthetic.main.fragment_inn_check.*

class InnCheckFragment : BaseFragment() {

    private var progressDialog: SpotsDialog? = null
    private val inputTinLimit = 14

    override fun getLayoutRes(): Int = R.layout.fragment_inn_check

    override fun getWindowSoftInputMode(): Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    companion object {
        private const val IS_CUSTOMER = "IS_CUSTOMER"
        fun getInstance(isCustomer: Boolean = false): InnCheckFragment {
            val args = Bundle()
            args.putBoolean(IS_CUSTOMER, isCustomer)
            val fragment = InnCheckFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = SpotsDialog(context, R.style.CustomSpotsDialog)
        UserManager.registeredUser.isCustomer = parseIsCustomerFromArgs()
        initViews()
    }

    private fun parseIsCustomerFromArgs() = arguments?.getBoolean(IS_CUSTOMER, false) ?: false

    private fun initViews() {

        val isCustomer = UserManager.registeredUser.isCustomer
        tv_title.setText(getMessageResId(isCustomer))
        civ_inn.setText(UserManager.registeredUser.innOrganisation)
        civ_inn.addTextWatcher(getInnWatcher())
        civ_inn.setOnEditorActionListener(TextView.OnEditorActionListener { _, p1, _ ->
            if (p1 == EditorInfo.IME_ACTION_DONE) {
                btn_next.performClick()
                return@OnEditorActionListener true
            }
            false
        })
        civ_inn.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            return@OnEditorActionListener if (actionId == EditorInfo.IME_ACTION_DONE) {
                btn_next.performClick()
                true
            } else {
                false
            }
        })
        btn_next.setOnClickListener { onBtnNextClick() }

        if (!isCustomer) {
            civ_inn.focus()
            civ_inn.postDelayed({ Helpers.showKeyboard(requireContext(), civ_inn) }, 100)
        }

    }

    private fun getMessageResId(isCustomer: Boolean): Int {
        return if (isCustomer) {
            R.string.put_company_inn
        } else {
            R.string.put_your_company_inn
        }
    }

    private fun getInnWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && p0.length > inputTinLimit) {
                    civ_inn.setText(p0.substring(0, inputTinLimit))
                    civ_inn.moveCursorToEnd()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                civ_inn.hideError()
            }

        }
    }

    private fun onBtnNextClick() {
        val inn = civ_inn.getText()

        if (inn.isBlank() || inn.length < inputTinLimit) {
            civ_inn.showError(getString(R.string.incorrect_inn_lenght_message))
            return
        }

        Helpers.hideKeyboard(requireContext(), civ_inn)

        requestUserInfo(inn)
    }

    private fun requestUserInfo(inn: String) {
        checkIsUserContain(inn) { exception, userExistCode ->
            exception?.let {
                showAlertMessage(R.string.error, exception.message)
            }

            userExistCode?.let {
                if (handleIsUserExist(it)) {
                    if (UserManager.registeredUser.isCustomer)
                        showCustomerContainError(inn)
                    else
                        showSignInFragment(inn)
                } else
                    requestInfoByTin(inn)
            }
        }
    }

    private fun showSignInFragment(inn: String) {
        val bundle = Bundle()
        bundle.putString(SignInFragment.INN, inn)
        val fragment = SignInFragment()
        fragment.arguments = bundle
        replaceFragment(fragment, true)
    }

    private fun handleIsUserExist(code: Int): Boolean {
        val isCustomer = UserManager.registeredUser.isCustomer
        return when (code) {
            UserExistCode.NO_EXIST.code -> false
            UserExistCode.EXIST_WITH_PARENT.code -> isCustomer
            UserExistCode.EXIST_WITHOUT_PARENT.code -> true
            else -> false
        }
    }

    private fun showCustomerContainError(inn: String) {
        civ_inn.showError(getString(R.string.user_exist_format, inn))
    }

    private fun checkIsUserContain(inn: String, callback: (OnoyException?, Int?) -> Unit) {
        val userExistCall = OnoyConnector.isUserExist(inn) { ex, existCode ->
            progressDialog?.dismiss()
            callback(ex, existCode)
        }

        progressDialog?.setOnCancelListener {
            userExistCall.cancel()
        }
        progressDialog?.show()
    }

    /**
     * Отображает AlertDialog c сообщением.
     * @param message сообщение для AlertDialog.
     */
    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    /**
     * Запрашивает у сервера налоговой информацию по ИНН.
     */
    private fun requestInfoByTin(inn: String) {

        val userInfoCall = OnoyConnector.getUserInfo(inn) { exception, user ->
            progressDialog?.dismiss()
            if (exception != null) {
                civ_inn.showError(exception.message)
                return@getUserInfo
            }

            if (user != null) {
                if (user.Status != UserStatuses.ok) {
                    handleIncorrectStatus(inn, user.Status)
                    return@getUserInfo
                }

                handleResponse(user)
                return@getUserInfo
            }

            civ_inn.showError(getString(R.string.bad_server_response_message))
        }
        progressDialog?.show()
        progressDialog?.setOnCancelListener {
            userInfoCall.cancel()
        }
    }

    private fun handleResponse(user: RegisteredUser) {
        setRegisterUser(user)
        val targetFragment = if (user.tax_agent) {
            TaxAgentRegisterInfoFragment()
        } else {
            RegistrationInfoFragment()
        }
        replaceFragment(targetFragment, true)
    }

    private fun setRegisterUser(user: RegisteredUser) {
        user.innOrganisation = UserManager.registeredUser.innOrganisation
        user.tax_agent = UserManager.registeredUser.tax_agent
        user.isCustomer = UserManager.registeredUser.isCustomer

        UserManager.registeredUser = user
    }

    private fun handleIncorrectStatus(inn: String, status: Int) {
        val message = when (status) {
            UserStatuses.notFound -> getString(R.string.tin_not_found_format, inn)
            UserStatuses.searchError -> getString(R.string.searching_error_message)
            else -> getString(R.string.not_specified_error_format, status)
        }
        civ_inn.showError(message)
    }
}
