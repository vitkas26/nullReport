package io.onteam.onoi.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import io.onteam.onoi.R
import io.onteam.onoi.utils.Helpers
import kotlinx.android.synthetic.main.activity_photo_view.*

class PhotoViewActivity : AppCompatActivity() {

    val layoutRes = R.layout.activity_photo_view

    companion object {
        private const val PHOTO = "PHOTO"
        fun getIntent(context: Context, base64photo: String): Intent {
            val intent = Intent(context, PhotoViewActivity::class.java)
            intent.putExtra(PHOTO, base64photo.toByteArray())
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            setContentView(layoutRes)
            updateToolbar()
            val b64String = String(intent.getByteArrayExtra(PHOTO))
            val base64ToBmp = Helpers.base64ToBmp(b64String)
            iv_photo.setImageBitmap(base64ToBmp)
        } catch (e: Exception) {
        }
    }

    private fun updateToolbar() {
        setSupportActionBar(centeredToolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
