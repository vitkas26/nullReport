package io.onteam.onoi.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;


public class EmptyToolbar extends Toolbar{

    public EmptyToolbar(Context context) {
        super(context);
    }

    public EmptyToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
