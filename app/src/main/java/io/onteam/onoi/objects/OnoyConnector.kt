package io.onteam.onoi.objects

import com.google.gson.JsonObject
import io.onteam.onoi.network.OnoiService
import okhttp3.MediaType
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import okhttp3.RequestBody


object OnoyConnector {

    private val onoiService = OnoiService()


    fun authCheck(inn: String, callback: ((OnoyException?, AuthCheckResponse?) -> Unit)): Call<AuthCheckResponse> {
        val jsonObj = JSONObject()
        jsonObj.put("inn", inn)
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObj.toString())

        val call = onoiService.getCloudPKIService().checkAuth(body)
        call.enqueue(object : Callback<AuthCheckResponse> {
            override fun onFailure(call: Call<AuthCheckResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback(OnoyException(t.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<AuthCheckResponse>, response: Response<AuthCheckResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }
        })
        return call
    }

    fun employeeCheck(innOrganisation: String, innEmployee: String, callback: ((OnoyException?, AuthEmployeeCheckResponse?) -> Unit)): Call<AuthEmployeeCheckResponse> {
        val jsonObj = JSONObject()
        jsonObj.put("innOrganisation", innOrganisation)
        jsonObj.put("innEmployee", innEmployee)
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObj.toString())

        val call = onoiService.getCloudPKIService().employeeCheck(body)
        call.enqueue(object : Callback<AuthEmployeeCheckResponse> {
            override fun onFailure(call: Call<AuthEmployeeCheckResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback(OnoyException(t.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<AuthEmployeeCheckResponse>, response: Response<AuthEmployeeCheckResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }
        })
        return call
    }

    fun pinConfirm(pinConfirm: PinConfirm, callback: ((OnoyException?, PinConfirmResponse?) -> Unit)): Call<PinConfirmResponse> {
        val jsonObj = JSONObject()
        jsonObj.put("pin", pinConfirm.pin)
        jsonObj.put("inn", pinConfirm.innOrganisation)

        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObj.toString())

        val call = onoiService.getCloudPKIService().pinConfirm(body)
        call.enqueue(object : Callback<PinConfirmResponse> {
            override fun onFailure(call: Call<PinConfirmResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback(OnoyException(t.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<PinConfirmResponse>, response: Response<PinConfirmResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }
        })
        return call
    }

    fun pinEmployeeConfirm(pinConfirm: PinConfirm, callback: ((OnoyException?, EmployeePinConfirmResponse?) -> Unit)): Call<EmployeePinConfirmResponse> {
        val jsonObj = JSONObject()
        jsonObj.put("pin", pinConfirm.pin)
        jsonObj.put("innOrganisation", pinConfirm.innOrganisation)
        jsonObj.put("innEmployee", pinConfirm.innEmployee)
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObj.toString())

        val call = onoiService.getCloudPKIService().pinEmployeeConfirm(body)
        call.enqueue(object : Callback<EmployeePinConfirmResponse> {
            override fun onFailure(call: Call<EmployeePinConfirmResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback(OnoyException(t.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<EmployeePinConfirmResponse>, response: Response<EmployeePinConfirmResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }
        })
        return call
    }

    fun signIn(signInfo: SignInfo, callback: ((OnoyException?, GetUserResponse?) -> Unit)): Call<GetUserResponse> {
        val call = onoiService.getOnoiService().getProfile(signInfo.inn, signInfo.keyword,
                signInfo.firebaseKey)
        call.enqueue(object : Callback<GetUserResponse> {
            override fun onFailure(call: Call<GetUserResponse>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<GetUserResponse>?, response: Response<GetUserResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }
        })
        return call
    }

    fun isUserExist(inn: String, callback: ((OnoyException?, Int?) -> Unit)): Call<RegistrationResponse> {
        val jsonObj = JsonObject()
        jsonObj.addProperty("inn", inn)

        val call = onoiService.getOnoiService().userExists(jsonObj.toString())

        call.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<RegistrationResponse>?, response: Response<RegistrationResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.code)
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return call
    }

    fun getUserInfo(inn: String, callback: (OnoyException?, RegisteredUser?) -> Unit): Call<RegisteredUser> {
        val userInfoCall = onoiService.getTaxService().getUserInfo(inn)
        userInfoCall.enqueue(object : Callback<RegisteredUser> {
            override fun onFailure(call: Call<RegisteredUser>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<RegisteredUser>?, response: Response<RegisteredUser>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return userInfoCall
    }

    fun getRegions(callback: (Array<Region>) -> Unit) {
        onoiService.getOnoiService().getRegions().enqueue(object : Callback<Array<Region>> {
            override fun onFailure(call: Call<Array<Region>>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Array<Region>>?, response: Response<Array<Region>>?) {
                if (response != null && response.isSuccessful && response.body() != null)
                    callback(response.body()!!)
            }
        })
    }

    fun registration(user: RegisteredUser, callback: ((OnoyException?, RegistrationResponse?) -> Unit)): Call<RegistrationResponse> {
        val registrationCall = onoiService.getOnoiService().registration(user.toJson())
        registrationCall.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<RegistrationResponse>?, response: Response<RegistrationResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body())
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return registrationCall
    }

    fun getNews(callback: (OnoyException?, Array<News>?) -> Unit): Call<Array<News>> {
        val newsCall = onoiService.getOnoiService().getNews()
        newsCall.enqueue(object : Callback<Array<News>> {
            override fun onFailure(call: Call<Array<News>>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), emptyArray())
                }
            }

            override fun onResponse(call: Call<Array<News>>?, response: Response<Array<News>>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!)
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return newsCall
    }

    fun setAutosentState(inn: String, isAutosentOn: Boolean, callback: (OnoyException?, Boolean) -> Unit): Call<BaseResponse> {
        val autosentCall = onoiService.getOnoiService().setAutosent(inn, isAutosentOn)
        autosentCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
                }
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.success)
                } else {
                    callback(getResponseException(response), false)
                }
            }
        })
        return autosentCall
    }

    fun sendMessage(message: ChatMessage, callback: (OnoyException?, Boolean) -> Unit) {
        val sendMessageCall = onoiService.getOnoiService().sendMessage(message.toJson())
        sendMessageCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.success)
                } else {
                    callback(getResponseException(response), false)
                }
            }
        })
    }

    fun getPdf(periodData: ReportPeriodData, callback: (OnoyException?, Boolean) -> Unit): Call<BaseResponse> {
        val getPdfCall = onoiService.getOnoiService().getPdf(periodData.email, periodData.inn,
                periodData.dateFrom, periodData.dateTo)
        getPdfCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.success)
                } else {
                    callback(getResponseException(response), false)
                }
            }
        })
        return getPdfCall
    }

    fun setInvitedBy(currentUserInn: String, informatorPhone: String, callback: (OnoyException?, Boolean) -> Unit): Call<BaseResponse> {
        val invitedByCall = onoiService.getOnoiService().setInvitedBy(currentUserInn, informatorPhone)
        invitedByCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.success)
                } else {
                    callback(getResponseException(response), false)
                }
            }

        })
        return invitedByCall
    }

    fun removeChild(inn: String, callback: (OnoyException?, Boolean) -> Unit): Call<BaseResponse> {
        val removeChildCall = onoiService.getOnoiService().removeChild(inn, null)
        removeChildCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.success)
                } else {
                    callback(getResponseException(response), false)
                }
            }

        })
        return removeChildCall
    }

    fun setAutosent(inn: String, isAutosent: Boolean, callback: (OnoyException?, Boolean) -> Unit): Call<BaseResponse> {
        val autosentCall = onoiService.getOnoiService().setAutosent(inn, isAutosent)
        autosentCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    callback(null, response.body()!!.success)
                } else {
                    callback(getResponseException(response), false)
                }
            }

        })
        return autosentCall
    }

    fun getAbonentArchive(inn: String, callback: (OnoyException?, Array<ArchiveGroup>?) -> Unit): Call<ArchivedReportResponse> {
        val abonentArchiveCall = onoiService.getOnoiService().getAbonentArchive(inn)
        abonentArchiveCall.enqueue(object : Callback<ArchivedReportResponse> {
            override fun onFailure(call: Call<ArchivedReportResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
            }

            override fun onResponse(call: Call<ArchivedReportResponse>?, response: Response<ArchivedReportResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    val reportResponse = response.body()!!
                    if (!reportResponse.success)
                        callback(OnoyException(reportResponse.message), null)
                    else
                        callback(null, reportResponse.forms)
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return abonentArchiveCall
    }

    fun getAbonentArchiveSync(inn: String) = onoiService.getOnoiService().getAbonentArchive(inn)

    fun getAbonentReports(inn: String, callback: (OnoyException?, Array<ReportGroup>?) -> Unit): Call<UserReportResponse> {
        val abonentReportsCall = onoiService.getOnoiService().getAbonentReports(inn)
        abonentReportsCall.enqueue(object : Callback<UserReportResponse> {
            override fun onFailure(call: Call<UserReportResponse>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
                }
            }

            override fun onResponse(call: Call<UserReportResponse>?, response: Response<UserReportResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    val reportResponse = response.body()!!
                    if (!reportResponse.success)
                        callback(OnoyException(reportResponse.message), null)
                    else
                        callback(null, reportResponse.forms)
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return abonentReportsCall
    }

    fun getAbonentReportsSync(inn: String) = onoiService.getOnoiService().getAbonentReports(inn)

    fun getFakeReports(callback: (OnoyException?, Array<ReportGroup>?) -> Unit): Call<UserReportResponse> {
        val fakeReportsCall = onoiService.getOnoiService().getFakeReports()
        fakeReportsCall.enqueue(object : Callback<UserReportResponse> {
            override fun onFailure(call: Call<UserReportResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), null)
            }

            override fun onResponse(call: Call<UserReportResponse>?, response: Response<UserReportResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    val reportResponse = response.body()!!
                    if (!reportResponse.success)
                        callback(OnoyException(reportResponse.message), null)
                    else
                        callback(null, reportResponse.forms)
                } else {
                    callback(getResponseException(response), null)
                }
            }

        })
        return fakeReportsCall
    }

    private fun isNetworkException(t: Throwable?): Boolean {
        return (t is UnknownHostException) || (t is TimeoutException) || (t is ConnectException)
    }

    fun saveAllReports(inn: String, callback: (OnoyException?, Boolean) -> Unit): Call<BaseResponse> {
        val saveAllReportsCall = onoiService.getOnoiService().saveAllReports(inn)
        saveAllReportsCall.enqueue(object : Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                if (call?.isCanceled == false)
                    callback(OnoyException(t?.message
                            ?: AppException.failure_response_message, isNetworkException(t)), false)
            }

            override fun onResponse(call: Call<BaseResponse>?, response: Response<BaseResponse>?) {
                if (response != null && response.isSuccessful && response.body() != null) {
                    val successSending = response.body()!!.success
                    callback(null, successSending)
                } else {
                    callback(getResponseException(response), false)
                }
            }
        })
        return saveAllReportsCall
    }

    fun getTop100Messages(id: Int, callback: (Array<ChatMessage>) -> Unit) {
        var messages = emptyArray<ChatMessage>()
        onoiService.getOnoiService().getTop100Messages(id)
                .enqueue(object : Callback<Array<ChatMessage>> {
                    override fun onFailure(call: Call<Array<ChatMessage>>?, t: Throwable?) {
                        callback(messages)
                    }

                    override fun onResponse(call: Call<Array<ChatMessage>>?, response: Response<Array<ChatMessage>>?) {
                        if (response != null && response.isSuccessful && response.body() != null) {
                            messages = response.body()!!
                        }
                        callback(messages)
                    }
                })
    }

    private fun <T> getResponseException(response: Response<T>?): OnoyException {
        var message = ""
        if (response == null) {
            return OnoyException(AppException.empty_response_message)
        }

        if (!response.isSuccessful) {

            response.errorBody()?.let {
                val j = JSONObject(it.string())
                if (j.has("message")) {
                    message = j.getString("message")
                }
            }

            if (message.isEmpty()) {
                message = response.message()

                if (message.isEmpty())
                    message = AppException.not_success_response_message
            }

            return OnoyException(message)
        }

        return OnoyException(AppException.unknown_message)
    }


}