package io.onteam.onoi.fragments

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.fragment_inner_subject_type.*

class SubjectTypeFragment : BaseFragment() {

    override fun getLayoutRes() = R.layout.fragment_inner_subject_type

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        btn_next.setOnClickListener { onNextBtnClick() }
    }

    private fun onNextBtnClick() {
        val type = getSubjectType()
        type?.let {
            UserManager.registeredUser.subjectType = type
            doRegistration()
            return
        }

        showAlertMessage(R.string.error, "Выберите тип")
    }

    private fun getSubjectType(): Subjects? {
        return when (rbgroup_subject_type.checkedRadioButtonId) {
            R.id.rb_medium_subject -> Subjects.MEDIUM
            R.id.rb_large_subject -> Subjects.LARGE
            else -> null
        }
    }

    private fun doRegistration() {
        replaceFragment(RegistrationInfoFragment(), true)
    }

    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }
}
