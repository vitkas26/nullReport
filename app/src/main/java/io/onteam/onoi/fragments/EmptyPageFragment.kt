package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.onteam.onoi.R


/**
 * Пустой Fragment. Используется как заглушка.
 */
class EmptyPageFragment : Fragment() {

    val layoutRes = R.layout.fragment_empty_page

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }
}
