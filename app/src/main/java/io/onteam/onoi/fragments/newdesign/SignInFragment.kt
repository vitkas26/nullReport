package io.onteam.onoi.fragments.newdesign


import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Helpers
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : BaseFragment() {

    companion object {
        const val INN = "INN"
    }

    private var progress: SpotsDialog? = null

    override fun getLayoutRes(): Int = R.layout.fragment_sign_in

    override fun getWindowSoftInputMode(): Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        progress = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)
        prepareInnView()
        preparePasswordView()
        btn_next.setOnClickListener { onNextBtnClick() }
    }

    private fun preparePasswordView() {
        val inn = tryParseInnFromArguments()
        if (inn.isNotEmpty()) {
            et_inn.setText(inn)
        }
    }

    private fun tryParseInnFromArguments(): String {
        return arguments?.getString(INN) ?: ""
    }

    private fun prepareInnView() {
        et_inn.addTextChangedListener(getInnWatcher())
        et_inn.setOnEditorActionListener(TextView.OnEditorActionListener { v, _, _ ->
            val length = v?.text?.length ?: 0
            if (length < 14) {
                Toast.makeText(requireContext(), R.string.incorrect_inn_lenght_message,
                        Toast.LENGTH_SHORT).show()
                return@OnEditorActionListener true
            }
            true
        })
    }

    private fun onNextBtnClick() {
        val inn = et_inn.text.toString()
        if (inn.isEmpty() || inn.length < 14) {
            Toast.makeText(requireContext(), getString(R.string.incorrect_inn_lenght_message), Toast.LENGTH_LONG).show()
            et_inn.requestFocus()
            return
        }

        Helpers.hideKeyboard(requireContext(), et_inn)
        authCheck(inn)
    }

    private fun authCheck(inn: String) {
        val signIn = OnoyConnector.authCheck(inn) { onoyException, response ->
            progress?.dismiss()

            if (response != null) {
                if (!response.success) {
                    handleException(response.message)
                    return@authCheck
                }
                handleResponse(inn, response.getNext())
            }

            if (onoyException != null) {
                handleException(onoyException.message)
            }
        }
        progress?.setOnCancelListener { signIn.cancel() }
        progress?.show()
    }

    private fun handleResponse(inn: String, nextStep: AuthNexts?) {
        UserManager.registeredUser.innOrganisation = inn

        val fragment = when (nextStep) {
            AuthNexts.give_me_personal_inn -> PersonalInnFragment()
            AuthNexts.give_me_pin -> PincodeInputFragment()
            else -> null
        }

        fragment?.let { replaceFragment(fragment, true) }
    }

    private fun handleException(message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(R.string.close, null)
                .create().show()
    }

    private fun getInnWatcher(): TextWatcher {
        val inputTinLimit = 14
        return object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

                btn_next.isEnabled = (p0 != null && p0.length == inputTinLimit)

                if (p0 != null && p0.length > inputTinLimit) {
                    et_inn.setText(p0.substring(0, inputTinLimit))
                    et_inn.setSelection(et_inn.text!!.length)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //et_inn.hideError()
            }
        }
    }
}
