package io.onteam.onoi.ui

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.R
import io.onteam.onoi.objects.ArchiveGroupedItem
import io.onteam.onoi.objects.ArchiveReport
import io.onteam.onoi.utils.DateHelper
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.report_adapter.view.*
import kotlinx.android.synthetic.main.report_title_view.view.*
import kotlinx.android.synthetic.main.report_view.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

/**
 * Created by sedi_user on 27.03.2018.
 */
class ArchiveReportsAdapter(val items: MutableList<ArchiveGroupedItem>,
                            private var changeListener: ReportsAdapter.ExpandChangeListener?) : RecyclerView.Adapter<ArchiveReportsAdapter.Holder>() {

    var lastCompanyName: String? = null
    private var lastExpandView: ExpandableLayout? = null
    private val layoutRes = R.layout.report_adapter


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(reportItemViewHolder: Holder, position: Int) {
        reportItemViewHolder.bindView(items[position])
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    inner class Holder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindView(item: ArchiveGroupedItem) {
            view.tv_company_name.text = item.company

            view.ll_all_report_sent.setVisible(false)

            if (isAdapterForTaxAgent()) {
                setHolderViewClickListener(item)
            } else {
                view.tv_company_name.gravity = Gravity.CENTER
                view.iv_arrow.setVisible(false)
                view.expl_reports.expand()
                insertReportsInView(item)
                notifyListener(item, true)
            }
        }

        private fun isAdapterForTaxAgent(): Boolean = changeListener != null

        private fun setHolderViewClickListener(item: ArchiveGroupedItem) {
            view.ll_report_title.setOnClickListener {
                val expandableLayout = view.expl_reports
                if (expandableLayout != lastExpandView) {
                    collapseLastExpandableLayout()
                }

                expandableLayout.toggle()
                if (expandableLayout.isExpanded) {
                    insertReportsInView(item)
                }
                notifyListener(item, expandableLayout.isExpanded)
                setLastExpandableLayout(expandableLayout)
            }
        }

        private fun setLastExpandableLayout(expandableLayout: ExpandableLayout?) {
            lastExpandView = expandableLayout
        }

        private fun notifyListener(item: ArchiveGroupedItem, isExpanded: Boolean) {
            lastCompanyName = if (isExpanded) item.company else null
            changeListener?.onViewExpandChange(lastCompanyName)
        }

        private fun collapseLastExpandableLayout() {
            lastExpandView?.setOnExpansionUpdateListener(null)
            lastExpandView?.collapse()
        }

        private fun insertReportsInView(item: ArchiveGroupedItem) {
            removeAllChildViews()

            val reports = item.reportsGroup
            if (reports.isEmpty()) {
                view.ll_all_report_sent.setVisible(true)
            } else {
                view.ll_all_report_sent.setVisible(false)
                reports.forEach {
                    createTitleAndInsetInView(it.name)
                    it.reports.forEach {
                        createReportAndInsetInView(it)
                    }
                }
            }
        }

        private fun removeAllChildViews() {
            view.ll_reports_holder.removeAllViews()
        }

        private fun createReportAndInsetInView(report: ArchiveReport) {
            val reportView = ReportItemView(view.context).getView(report)
            view.ll_reports_holder.addView(reportView)
        }

        private fun createTitleAndInsetInView(name: String) {
            val titleView = TitleItemView(view.context).getView(name)
            view.ll_reports_holder.addView(titleView)
        }
    }

    inner class TitleItemView(val context: Context) {
        fun getView(name: String): View {
            val view = LayoutInflater.from(context).inflate(R.layout.report_title_view, null, false)
            view.tv_supervisor_name.text = name
            return view
        }
    }

    inner class ReportItemView(val context: Context) {
        fun getView(report: ArchiveReport): View {
            val view = LayoutInflater.from(context).inflate(R.layout.report_view, null, false)

            val color = ContextCompat.getColor(view.context,
                    getTintColorByStatus(report.report_status_id))

            view.tv_id_name_title.text = report.name
            view.tv_description.text = report.description
            view.tv_period.text = getPeriod(context, report.datetime)
            view.tv_period_end.visibility = View.GONE

            view.iv_icon.setColorFilter(color)
            view.iv_icon.setImageDrawable(ContextCompat.getDrawable(view.context,
                    getStatusImageRes(report.report_status_id)))

            view.tv_report_status.setTextColor(color)
            view.tv_report_status.text = view.context
                    .getString(getStatusName(report.report_status_id))

            return view
        }

        private fun getPeriod(context: Context, datetime: String): String {
            val date = DateHelper.parseDate(datetime)
            val calendar = Calendar.getInstance()
            calendar.time = date

            val monthArray = context.resources.getStringArray(R.array.month)
            val month = monthArray[calendar.get(Calendar.MONTH)]
            val year = calendar.get(Calendar.YEAR)
            return context.getString(R.string.period_format, month, year)
        }

        private fun getStatusImageRes(report_status_id: Int?): Int {
            return when (report_status_id) {
                10 -> R.drawable.ic_check_circle_black_24dp
                20 -> R.drawable.ic_warning_black_24dp
                else -> R.drawable.ic_autorenew_black_24dp
            }
        }

        private fun getStatusName(report_status_id: Int?): Int {
            return when (report_status_id) {
                10 -> R.string.report_accepted
                20 -> R.string.report_rejected
                else -> R.string.report_in_process
            }
        }

        private fun getTintColorByStatus(report_status_id: Int?): Int {
            return when (report_status_id) {
                10 -> R.color.colorGreen
                20 -> R.color.colorRed
                else -> R.color.colorLiteBlue
            }
        }

    }
}