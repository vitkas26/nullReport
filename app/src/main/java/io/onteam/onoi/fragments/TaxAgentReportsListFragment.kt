package io.onteam.onoi.fragments


import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dmax.dialog.SpotsDialog
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.CustomerRegistrationActivity
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.network.OnoiService
import io.onteam.onoi.objects.*
import io.onteam.onoi.ui.ReportsAdapter
import io.onteam.onoi.utils.DateHelper
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.fragment_tax_user_reports_list.*
import kotlinx.android.synthetic.main.fragment_tax_user_reports_list.view.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch


/**
 *  Fragment используемый для отображения списка отчетов пользователя.
 */
class TaxAgentReportsListFragment : BaseReportFragment(), FragmentVisibleListener, UserManager.UserManagerChangeListener, ReportsAdapter.ExpandChangeListener {

    private val layoutRes = R.layout.fragment_tax_user_reports_list

    //Отчеты пользователя.
    private val reports = mutableListOf<ReportGroupedItem>()
    //Адаптер для списка отчетов.
    private val adapter = ReportsAdapter(reports, this)
    //Retrofit сервис.
    private val onoiService = OnoiService()

    private var progress: Dialog? = null
    private var isVisibleNow = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        initViews(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress = SpotsDialog(context, R.style.CustomSpotsDialog)

        UserManager.changeListeners.add(this)

        btn_move_to_profile.setOnClickListener {
            startActivity(CustomerRegistrationActivity.getIntent(requireContext()))
        }

        requestInfo()
    }

    override fun onDestroyView() {
        UserManager.changeListeners.remove(this)
        super.onDestroyView()
    }

    /**
     * Инициализация компонентов fragment'a.
     * @param view родительский View.
     */
    private fun initViews(view: View) {
        view.refresh_layout.setColorSchemeResources(R.color.colorAccent)
        view.refresh_layout.setOnRefreshListener {
            collapseExpandableItem()
            requestInfo()
        }

        UserManager.getCurrentUser()?.let {
            view.tv_company_name.text = it.name
        }

        view.rv_reports.adapter = adapter
        view.rv_reports.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun collapseExpandableItem() {
        (rv_reports.adapter as? ReportsAdapter)?.collapseItemAndReset()
    }

    /**
     * Запращивает список отчетов.
     */
    private fun requestInfo(callback: Runnable? = null) {
        val user = UserManager.getCurrentUser() ?: return
        requestTaxUserClientInfo(user, callback)
    }

    private fun requestTaxUserClientInfo(user: UserProfile, callback: Runnable?) =
            launch(UI) {
                refresh_layout.isRefreshing = true
                reports.clear()

                user.children?.forEach { user ->
                    val reportResponse = getUser(user.inn).await()

                    val filterReportsGroup = mutableListOf<ReportGroup>()

                    reportResponse?.forms?.forEach { group ->
                        val filteredReports = filterReports(group.reports)
                        filterReportsGroup.add(ReportGroup(group.name, filteredReports))
                    }

                    filterReportsGroup.forEach {
                        reports.add(ReportGroupedItem(user.name, filterReportsGroup.toTypedArray()))
                    }
                }

                if (isVisibleNow)
                    restoreFabState()

                callback?.run()
                adapter.notifyDataSetChanged()

                ab_title.setVisible(user.hasCustomer())
                refresh_layout.setVisible(user.hasCustomer())
                ll_empty_customer_layout.setVisible(user.tax_agent && !user.hasCustomer())
                tv_empty_list_message.setVisible(user.hasCustomer() && reports.isEmpty())
                refresh_layout.isRefreshing = false
            }


    private fun getUser(inn: String) =
            async(CommonPool) {
                val response = OnoyConnector.getAbonentReportsSync(inn).execute()
                if (response != null && response.isSuccessful) response.body()
                else null
            }

    /**
     * Фильтруем отчеты пользователя по месяцам. Если сейчас месяц который содердится в массиве {@code months}
     * в отчете.
     * @param  originalReports отчеты пользователя.
     */
    private fun filterReports(originalReports: Array<Report>): Array<Report> {
        val currentMonth = DateHelper.getCurrentMonth()
        return originalReports.filter {
            it.months.contains(currentMonth)
        }.toTypedArray()
    }


    private fun sendAllReports(inn: String) {
        val saveAllReportsCall = OnoyConnector.saveAllReports(inn) { onoyException, isSuccess ->
            progress?.dismiss()

            onoyException?.apply {
                showAlertDialog(message)
            }

            if (isSuccess) {
                collapseExpandableItem()
                requestInfo(Runnable { notifyReportListChangeListener() })
                Toast.makeText(context, R.string.all_reports_sending_success, Toast.LENGTH_SHORT).show()
            }
        }

        progress?.setOnCancelListener { saveAllReportsCall.cancel() }
        progress?.show()
    }

    private fun showAlertDialog(message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    override fun currentUserInfoChange() {
        requestInfo()
    }

    override fun setVisible() {
        isVisibleNow = true
        restoreFabState()
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }

    private fun restoreFabState() {
        val companyName = (rv_reports.adapter as ReportsAdapter).lastCompanyName
        val children = UserManager.getChildrenByName(companyName)
        val action = getActionForFabClick(children)
        updateFabVisibility(action)
    }

    override fun onViewExpandChange(companyName: String?) {
        if (companyName != null) {
            scrollViewToItem(companyName)
        }
        val children = UserManager.getChildrenByName(companyName)
        val action = getActionForFabClick(children)
        updateFabVisibility(action)
    }

    private fun scrollViewToItem(companyName: String?) {
        val item = adapter.items.find { it.company == companyName }
        val index = adapter.items.indexOf(item)
        rv_reports.scrollToPosition(index)
    }

    private fun updateFabVisibility(runnable: Runnable?) {
        (activity as? MainActivity)?.updateFab(
                FabUpdateEvent(R.drawable.ic_send_black_24dp, FabUpdateEvent.Type.SendReport, runnable))
    }

    private fun getActionForFabClick(children: UserProfile?): Runnable? {
        return if (children == null) {
            null
        } else {
            Runnable {
                showSendConfirmationDialog(children)
            }
        }
    }


    private fun showSendConfirmationDialog(profile: UserProfile) {
        val message = getString(R.string.send_company_report_confirmation_message, profile.name)
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setTitle(R.string.confirm_action)
                .setMessage(message)
                .setPositiveButton(R.string.send) { _, _ ->
                    sendAllReports(profile.inn)
                }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }


    private fun notifyReportListChangeListener() {
        changeListener?.reloadData()
    }
}
