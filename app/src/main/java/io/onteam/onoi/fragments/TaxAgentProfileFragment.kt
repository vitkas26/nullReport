package io.onteam.onoi.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dmax.dialog.SpotsDialog
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.CustomerRegistrationActivity
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.objects.FabUpdateEvent
import io.onteam.onoi.objects.LocalBroadcatActions
import io.onteam.onoi.objects.OnoyConnector
import io.onteam.onoi.objects.UserManager
import io.onteam.onoi.ui.CustomerAdapter
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.fragment_tax_agent_profile.*

class TaxAgentProfileFragment : Fragment(), FragmentVisibleListener,
        CustomerAdapter.OnCustomerChangeListener, UserManager.UserManagerChangeListener {

    private val layoutRes = R.layout.fragment_tax_agent_profile
    private var isVisibleNow = false
    private var customerAdapter: CustomerAdapter? = null

    private var balanceChangeReceiver: BroadcastReceiver? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    private fun getCustomerAdapter(): CustomerAdapter? {
        val customers = UserManager.getCurrentUser()?.children ?: return null
        customerAdapter = CustomerAdapter(customers)
        customerAdapter!!.changeListener = this
        return customerAdapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        UserManager.changeListeners.add(this)

        UserManager.getCurrentUser()?.let {
            updateBalance(it.balance)
            tv_company_name.text = it.name

            rv_customers.layoutManager = LinearLayoutManager(requireContext())
            rv_customers.adapter = getCustomerAdapter()
        }

        rl_exit.setOnClickListener { removeUser() }

        registerBalanceReceiver()
    }

    private fun removeUser() {
        AlertDialog.Builder(requireContext(), R.style.ExitAlertDialogTheme)
                .setTitle(R.string.confirm_action)
                .setMessage(R.string.exit_query_message)
                .setPositiveButton(R.string.cancel, null)
                .setNegativeButton(R.string.exit) { _, _ ->
                    run {
                        UserManager.resetCurrentUser()

                        Preferences.setValue(Preferences.Keys.USER_KEYWORD, "")
                        Preferences.setValue(Preferences.Keys.USER_INN, "")

                        requireActivity().finish()
                    }
                }
                .create().show()
    }


    override fun onDestroyView() {
        unregisterReceiver()
        UserManager.changeListeners.remove(this)
        super.onDestroyView()
    }

    private fun unregisterReceiver() {
        val broadcastManager = LocalBroadcastManager.getInstance(requireContext())
        try {
            balanceChangeReceiver?.let {
                broadcastManager.unregisterReceiver(it)
            }
        } catch (ignored: Exception) {
        }
    }

    private fun registerBalanceReceiver() {
        balanceChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let {
                    val double = it.getDoubleExtra("balance", 0.0)
                    updateBalance(double)
                }
            }
        }

        LocalBroadcastManager.getInstance(requireContext())
                .registerReceiver(balanceChangeReceiver!!,
                        IntentFilter(LocalBroadcatActions.CHANGE_BALANCE))
    }

    private fun updateBalance(balance: Double) {
        tv_balance?.apply {
            text = getString(R.string.balance_format, balance).toUpperCase()
        }
    }

    override fun currentUserInfoChange() {
        refreshDataInRecyclerView()
    }

    private fun refreshDataInRecyclerView() {
        if (customerAdapter != null)
            customerAdapter?.notifyDataSetChanged()
        else
            rv_customers.adapter = getCustomerAdapter()
    }

    override fun setVisible() {
        setVisibleNowState(true)
        (activity as? MainActivity)?.updateFab(getFabEvent())
    }

    private fun getFabEvent(): FabUpdateEvent {
        return FabUpdateEvent(
                R.drawable.ic_person_add_black_24dp,
                FabUpdateEvent.Type.CustomerRegistration,
                getClickAction())
    }

    private fun getClickAction() = Runnable {
        startActivity(CustomerRegistrationActivity.getIntent(requireContext()))
    }

    private fun setVisibleNowState(isVisible: Boolean) {
        isVisibleNow = isVisible
    }

    override fun setUnvisible() {
        setVisibleNowState(false)
    }

    override fun onRemove(inn: String, callback: (Boolean) -> Unit) {
        AlertDialog.Builder(requireContext(), R.style.RemoveAlertDialogTheme)
                .setTitle(R.string.confirm_action)
                .setMessage(R.string.remove_customer_query_message)
                .setPositiveButton(R.string.remove) { _, _ -> removeCustomer(inn, callback) }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }

    private fun removeCustomer(inn: String, callback: (Boolean) -> Unit) {
        val dialog = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)

        val removeChildCall = OnoyConnector.removeChild(inn) { onoyException, isSuccess ->
            dialog.dismiss()
            if (onoyException != null) {
                showAlertMessage(getString(R.string.error), onoyException.message)
                return@removeChild
            }
            if (isSuccess)
                UserManager.removeChildren(inn)
            callback(isSuccess)
        }

        dialog.setOnCancelListener { removeChildCall.cancel() }
        dialog.show()
    }

    private fun showAlertMessage(title: String, message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    override fun onChangeAutoSend(inn: String, isEnabled: Boolean, callback: (Boolean) -> Unit) {
        val dialog = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)

        val autosentCall = OnoyConnector.setAutosent(inn, isEnabled) { onoyException, isSuccess ->
            dialog.dismiss()
            if (onoyException != null) {
                showAlertMessage(getString(R.string.error), onoyException.message)
                return@setAutosent
            }
            callback(isSuccess)
        }
        dialog.setOnCancelListener { autosentCall.cancel() }
        dialog.show()
    }
}
