package io.onteam.onoi.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import io.onteam.onoi.R
import io.onteam.onoi.objects.Region
import kotlinx.android.synthetic.main.item_region_layout.view.*

class RegionSpinnerAdapter(context: Context, val objects: Array<Region>)
    : ArrayAdapter<Region>(context, R.layout.item_region_layout, objects) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_region_layout,
                parent, false)
        val region = objects[position]
        view.tv_region_name.text = region.toString()
        return view
    }
}