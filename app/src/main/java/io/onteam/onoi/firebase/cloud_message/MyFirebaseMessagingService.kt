package io.onteam.onoi.firebase.cloud_message

import android.support.v4.content.LocalBroadcastManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.onteam.onoi.utils.Preferences

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private lateinit var remoteMessageHandler: RemoteMessageHandler
    private lateinit var localBroadcastReceiver: LocalBroadcastManager

    override fun onCreate() {
        super.onCreate()

        localBroadcastReceiver = LocalBroadcastManager.getInstance(this)
        remoteMessageHandler = RemoteMessageHandler(localBroadcastReceiver)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        remoteMessage?.let {
            remoteMessageHandler.handleMessage(it)
        }
    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        p0?.let {
            Preferences.setValue(Preferences.Keys.FIREBASE_KEY, it)
        }
    }
}