package io.onteam.onoi.ui

import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import io.onteam.onoi.R

/**
 * Created by sedi_user on 27.03.2018.
 */
class BottomNavigationPresenter(private val bottom_nav: AHBottomNavigation, val activity: AppCompatActivity, tabSelectedListener: AHBottomNavigation.OnTabSelectedListener) {

    init {
        initBottomNavigation()
        bottom_nav.setOnTabSelectedListener(tabSelectedListener)
    }

    /**
     * Устанавливает начальные настройки для {@link AHBottomNavigation}.
     */
    private fun initBottomNavigation() {
        val adapter = AHBottomNavigationAdapter(activity, R.menu.bottom_main_menu)
        adapter.setupWithBottomNavigation(bottom_nav)

        bottom_nav.defaultBackgroundColor = ContextCompat.getColor(activity, R.color.backgroundColor)
        bottom_nav.accentColor = ContextCompat.getColor(activity, R.color.colorPrimary)
        bottom_nav.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
    }

    /**
     * Устанавливает нужный элемент в {@link AHBottomNavigation} по {@see position}
     *  @param position позиция нужного элемента.
     *  @param useCallback нужно ли вызывать callback после выделения элемента.
     */
    fun setSelectedItem(position: Int, useCallback: Boolean = false) {
        bottom_nav.setCurrentItem(position, useCallback)
    }
}