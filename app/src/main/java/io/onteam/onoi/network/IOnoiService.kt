package io.onteam.onoi.network

import io.onteam.onoi.objects.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Описывает функции доступные для использования в системе Оной.
 */
interface IOnoiService {

    companion object {
        //Ссылка на систему
        const val URL = "http://curator.onoi.kg/api.php/"
    }

    //region Form queries
    /**
     * Проверяет был ли пользователь ранее зарегистрирован.
     * @param data структура в json объекта {@link RegisteredUser}
     */
    @FormUrlEncoded
    @POST("user-exists")
    fun userExists(@Field("data") data: String): Call<RegistrationResponse>

    /**
     * Регистрирует нового пользователя в системе.
     * @param data структура в json объекта {@link RegisteredUser}
     */
    @FormUrlEncoded
    @POST("registration")
    fun registration(@Field("data") data: String): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("edit-parent/{inn}")
    fun removeChild(@Path("inn") inn: String, @Field("data") data: String?): Call<BaseResponse>

    @FormUrlEncoded
    @POST("chat/new/abonent/")
    fun sendMessage(@Field("data") data: String?): Call<BaseResponse>

    /**
     * Сохраняет все отчеты.
     * @param inn - ИНН пользователя.
     */
    @FormUrlEncoded
    @POST("save-reports")
    fun saveAllReports(@Field("inn") inn: String): Call<BaseResponse>

    @FormUrlEncoded
    @POST("set-invited-by")
    fun setInvitedBy(@Field("inn") userInn: String, @Field("phone") informatorPhone: String): Call<BaseResponse>

    @FormUrlEncoded
    @POST("set-auto-sent")
    fun setAutosent(@Field("inn") userInn: String, @Field("auto_sent") isAutosent: Boolean): Call<BaseResponse>

    //endregion

    //region Get queries

    @GET("get-pdf")
    fun getPdf(@Query("email") email: String, @Query("inn") inn: String,
               @Query("from") from: String, @Query("to") to: String): Call<BaseResponse>

    /**
     * Возвращает все новости.
     */
    @GET("get-zones")
    fun getRegions(): Call<Array<Region>>

    /**
     * Возвращает все новости.
     */
    @GET("get-news")
    fun getNews(@Query("language") lang: String = "ru"): Call<Array<News>>

    /**
     * Возвращает все привязанные к пользователю отчеты.
     * @param inn - ИНН пользователя.
     */
    @GET("get-abonent-form/{inn}")
    fun getAbonentReports(@Path("inn") inn: String, @Query("language") lang: String = "ru"): Call<UserReportResponse>

    /**
     * Возвращает все отчеты.
     */
    @GET("get-all-forms")
    fun getFakeReports(@Query("language") lang: String = "ru"): Call<UserReportResponse>

    /**
     * Возвращает все отчеты которые находятся в архиве.
     * @param inn - ИНН пользователя.
     */
    @GET("get-archive/{inn}")
    fun getAbonentArchive(@Path("inn") inn: String): Call<ArchivedReportResponse>

    /**
     * Возвращает информацию по авторизуемому пользователю.
     * @param inn - ИНН пользвателя.
     * @param secret - кодовое слово указанное при регистрации.
     */
    @GET("get-profile/{inn}")
    fun getProfile(@Path("inn") inn: String, @Query("secret") secret: String, @Query("firebase") firebaseToken: String): Call<GetUserResponse>

    @GET("chat/get-messages/{id}")
    fun getTop100Messages(@Path("id") id: Int): Call<Array<ChatMessage>>

    //endregion

}