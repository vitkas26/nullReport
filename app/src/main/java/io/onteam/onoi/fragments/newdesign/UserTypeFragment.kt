package io.onteam.onoi.fragments.newdesign


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.fragments.TaxRegimeFragment
import io.onteam.onoi.objects.UserManager
import kotlinx.android.synthetic.main.fragment_user_type.*

class UserTypeFragment : BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_user_type

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        btn_single_user.setOnClickListener { onSingleUserClick() }
        btn_tax_user.setOnClickListener { onTaxUserClick() }
    }

    private fun onTaxUserClick() {
        setIsTaxAgent(true)
        goToRegistration()
    }

    private fun onSingleUserClick() {
        setIsTaxAgent(false)
        goToRegistration()
    }

    private fun goToRegistration() {
        val targetFragment: Fragment?
        if (UserManager.registeredUser.tax_agent) {
            targetFragment = TaxAgentRegisterInfoFragment()
        } else {
            targetFragment = TaxRegimeFragment()
        }

        replaceFragment(targetFragment, true)
    }

    private fun setIsTaxAgent(isTaxAgent: Boolean) {
        UserManager.registeredUser.tax_agent = isTaxAgent
    }
}
