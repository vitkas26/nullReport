package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.OnReportListChangeListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.dialogs.EmailPeriodDialog
import io.onteam.onoi.objects.*
import io.onteam.onoi.ui.ArchiveReportsAdapter
import io.onteam.onoi.ui.TextSizedToast
import kotlinx.android.synthetic.main.fragment_archive_reports_list.*
import kotlinx.android.synthetic.main.fragment_archive_reports_list.view.*
import java.util.*


/**
 *  Fragment используемый для отображения списка отчетов находящихся в архиве.
 */
class ArchiveReportsListFragment : Fragment(), OnReportListChangeListener, FragmentVisibleListener {

    private val TAG: String = "ArchiveReportsListFrgm"
    private val layoutRes = R.layout.fragment_archive_reports_list


    //Отчеты из архива.
    private val reports = mutableListOf<ArchiveGroupedItem>()

    //Адаптер для списка отчетов.
    private val adapter = ArchiveReportsAdapter(reports, null)

    private var monitoringTimer = Timer()
    private var timerIsRunning = false
    private var isVisibleNow = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        initViews(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestInfo()
    }

    override fun onDestroyView() {
        cancelMonitoringTimer()
        super.onDestroyView()
    }

    private fun showEmailPeriodDialog() {
        EmailPeriodDialog().show(requireFragmentManager())
    }

    override fun reloadData() {
        requestInfo(true)
    }

    /**
     * Запускает таймер для мониторинга состояния отчетов.
     * Делаем опрос раз в 1 минуту
     */
    private fun runMonitoringTimer() {
        if (timerIsRunning)
            return

        monitoringTimer.schedule(object : TimerTask() {
            override fun run() {
                requestInfo(true)
            }
        }, 60 * 1000, 60 * 1000)

        timerIsRunning = true
    }

    /**
     * Запращивает список отчетов из архива.
     */
    private fun requestInfo(isReload: Boolean = false) {
        val user = UserManager.getCurrentUser()

        if (user == null) {
            setEmptyPreload(EmptyPreload.getNoSended())
            return
        }

        OnoyConnector.getAbonentArchive(user.inn) { onoyException, arrayOfArchivedObjects ->

            resetEmptyLayout()

            onoyException?.apply {
                Toast.makeText(context, "Архив: $message", Toast.LENGTH_SHORT).show()
                if (isReload)
                    updateFabVisibility()
                if (isNetworkException) {
                    reports.clear()
                    adapter.notifyDataSetChanged()
                    setEmptyPreload(EmptyPreload.getNoNetwork())
                }
            }

            arrayOfArchivedObjects?.let { it ->
                reports.clear()

                var hasProcessedReport = false

                if (it.isNotEmpty()) {
                    it.forEach { archiveObj ->
                        archiveObj.reports.sortByDescending { it.datetime }
                        hasProcessedReport = archiveObj.reports.any { it.report_status_id == null }
                    }
                    reports.add(ArchiveGroupedItem(user.name, it))
                }

                adapter.notifyDataSetChanged()

                if (hasProcessedReport)
                    runMonitoringTimer()
                else
                    cancelMonitoringTimer()

                //Если нет отчетов для показывем
                if (reports.isEmpty())
                    setEmptyPreload(EmptyPreload.getNoSended())

                if (isReload && isVisibleNow) {
                    updateFabVisibility()
                }
            }
        }
    }

    private fun setEmptyPreload(preload: EmptyListPreload) {
        el_message.setPreload(preload)
    }

    private fun resetEmptyLayout() {
        el_message.resetPreload()
    }

    /**
     * Останавливает таймер мониторинга состояния отчетов.
     */
    private fun cancelMonitoringTimer() {
        if (!timerIsRunning)
            return
        monitoringTimer.cancel()
        timerIsRunning = false
        Log.i(TAG, "Monitoring timer is cancelled")
    }

    override fun setVisible() {
        isVisibleNow = true
        updateFabVisibility()
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }

    private fun updateFabVisibility() {
        var runnable: Runnable? = Runnable { showEmailPeriodDialog() }
        if (reports.isEmpty()) {
            runnable = null
        }
        (activity as? MainActivity)?.updateFab(FabUpdateEvent(R.drawable.ic_mail_black_24dp,
                FabUpdateEvent.Type.SendArchive, runnable))
    }

    /**
     * Инициализация компонентов fragment'a.
     * @param view родительский View.
     */
    private fun initViews(view: View) {
        view.rv_reports.adapter = adapter
        view.rv_reports.layoutManager = LinearLayoutManager(context)

        view.refresh_layout.setColorSchemeResources(R.color.colorAccent)
        view.refresh_layout.setOnRefreshListener {

            val hasProcessingReport = reports.any { it ->
                it.reportsGroup.any { it ->
                    it.reports.any { it.report_status_id == null }
                }
            }

            if (hasProcessingReport) {
                TextSizedToast(context!!, 18f).show(R.string.archive_request_timeout_message,
                        Toast.LENGTH_LONG)
            }
            refresh_layout.isRefreshing = false
        }
    }
}
