package io.onteam.onoi.objects

import com.google.gson.JsonObject


data class ChatMessage(var inn: String, var message: String, var is_user: Boolean){

    fun toJson(): String {
        val json = JsonObject()
        json.addProperty("inn", inn)
        json.addProperty("message", message)
        return json.toString()
    }
}