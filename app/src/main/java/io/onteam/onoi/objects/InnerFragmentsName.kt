package io.onteam.onoi.objects

enum class InnerFragmentsName {
    SIGN_IN,
    REGISTRATION,
    TAX_REGIME_TYPE,
    SUBJECT_TYPE
}