package io.onteam.onoi.objects

import org.json.JSONObject
import java.io.Serializable

/**
 * Класс описывает структуру пользователя который будет использоваться
 * для при регистрации ноовго пользователя.
 */
class RegisteredUser : Serializable {


    var Address: String = ""
    var Name: String = ""
    var Rayon: String = ""
    var innOrganisation: String = ""
    var innEmployee: String = ""
    var okpo: Int = 0
    var chief_pin: String = ""
    var chief: String = ""
    var accountant_pin: String = ""
    var accountant_name: String = ""
    var social_number: String = ""
    var region_id: Int = 0
    var tax_agent: Boolean = false
    var parent: String = ""
    var isCustomer = false
    var taxRegime: TaxRegimes? = null
    var subjectType: Subjects? = null
    var Status: Int = -1

    fun getRegionCode(): String {
        return Rayon.substring(0, 3)
    }


    fun isIndividualUser(): Boolean {
        val leadNum = innOrganisation[0]
        return leadNum == '1' || leadNum == '2'
    }

    /**
     * Возвращает строку в JSON для регистрируемого пользователя.
     */
    fun toJson(): String {
        val user = JSONObject()

        // OLD VARS
        user.put("passport_front", "")
        user.put("passport_back", "")
        user.put("certificate", "")
        user.put("sf_photo", "")
        user.put("secret", "0")
        user.put("phone", "")

        // ---
        user.put("name", Name)
        user.put("inn", innOrganisation)
        user.put("region", getRegionCode())
        user.put("okpo", "123456789")
        user.put("region_id", region_id)
        user.put("address", Address)

        user.put("chief_pin", chief_pin)
        user.put("chief", chief)

        user.put("accountant_pin", accountant_pin)
        user.put("accountant_name", accountant_name)
        user.put("social_number", social_number)

        if (tax_agent)
            user.put("tax_agent", tax_agent)

        taxRegime?.let {
            user.put("tax_regime", it.type)
        }

        subjectType?.let {
            user.put("subject_type", it.type)
        }

        user.put("parent", parent)
        return user.toString()
    }
}