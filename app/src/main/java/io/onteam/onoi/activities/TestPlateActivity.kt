package io.onteam.onoi.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import io.onteam.onoi.R
import io.onteam.onoi.ui.TextSizedToast
import kotlinx.android.synthetic.main.activity_test_plate.*

class TestPlateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_plate)

        button.setOnClickListener {
            TextSizedToast(this, 18f).show(R.string.report_sending_success)
        }
    }
}
