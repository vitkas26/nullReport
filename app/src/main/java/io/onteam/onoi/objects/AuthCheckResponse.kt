package io.onteam.onoi.objects

class AuthCheckResponse : BaseResponse() {
    val inn: String? = null
    private val next: String? = null

    fun getNext(): AuthNexts? {
        return when (next) {
            "give-me-pin" -> AuthNexts.give_me_pin
            "give-me-personal-inn" -> AuthNexts.give_me_personal_inn
            else -> null
        }
    }


}