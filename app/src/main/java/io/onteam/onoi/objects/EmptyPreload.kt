package io.onteam.onoi.objects

import io.onteam.onoi.R

object EmptyPreload {
    fun getSended() = SendedEmptyPreload()
    fun getNoSended() = NoSendedEmptyPreload()
    fun getNoNetwork() = NoNetworkEmptyPreload()
}


class SendedEmptyPreload
    : EmptyListPreload(R.string.congratulations, R.string.report_sended_success_in_interval, R.drawable.success_sended)

class NoNetworkEmptyPreload
    : EmptyListPreload(R.string.no_connect, R.string.refresh_page, R.drawable.no_wifi)

class NoSendedEmptyPreload
    : EmptyListPreload(0, R.string.report_no_sended, R.drawable.empty)