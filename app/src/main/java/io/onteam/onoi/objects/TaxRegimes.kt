package io.onteam.onoi.objects

enum class TaxRegimes(val type: Int) {
    WITH_NDS(0),
    NO_NDS(1),
    UNIFIED(2)
}