package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.objects.*
import kotlinx.android.synthetic.main.fragment_inner_select_tax_regime.*

class TaxRegimeFragment : BaseFragment() {
    override fun getLayoutRes() = R.layout.fragment_inner_select_tax_regime

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        btn_next.setOnClickListener {
            onNextBtnClick()
        }
    }

    private fun onNextBtnClick() {
        val type = getTaxRegimeType()
        type?.let {
            UserManager.registeredUser.taxRegime = it
            if (it == TaxRegimes.UNIFIED)
                setUserInfoFragment()
            else
                setSubjectTypeFragment()
            return
        }

        showAlertMessage(R.string.error, "Выберите тип")
    }

    private fun setSubjectTypeFragment() {
        replaceFragment(SubjectTypeFragment(), true)
    }

    private fun setUserInfoFragment() {
        replaceFragment(RegistrationInfoFragment(), true)
    }

    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    private fun getTaxRegimeType(): TaxRegimes? {
        val checkedRadioButtonId = rbgroup_payer_type.checkedRadioButtonId
        return when (checkedRadioButtonId) {
            R.id.rb_nds_payer -> TaxRegimes.WITH_NDS
            R.id.rb_nds_no_payer -> TaxRegimes.NO_NDS
            R.id.rb_single_tax -> TaxRegimes.UNIFIED
            else -> null
        }
    }


}
