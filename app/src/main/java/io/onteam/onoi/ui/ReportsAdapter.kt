package io.onteam.onoi.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.R
import io.onteam.onoi.objects.Report
import io.onteam.onoi.objects.ReportGroupedItem
import io.onteam.onoi.utils.DateHelper
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.report_adapter.view.*
import kotlinx.android.synthetic.main.report_title_view.view.*
import kotlinx.android.synthetic.main.report_view.view.*
import net.cachapa.expandablelayout.ExpandableLayout

class ReportsAdapter(var items: MutableList<ReportGroupedItem>,
                     private var changeListener: ExpandChangeListener?) : RecyclerView.Adapter<ReportsAdapter.Holder>() {

    private val layoutRes = R.layout.report_adapter
    var lastCompanyName: String? = null
    private var lastExpandView: ExpandableLayout? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportsAdapter.Holder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    fun collapseItemAndReset() {
        lastExpandView?.collapse()
        changeListener?.onViewExpandChange(null)
        lastExpandView = null
        lastCompanyName = null
    }

    override fun onBindViewHolder(reportItemViewHolder: ReportsAdapter.Holder, position: Int) {
        reportItemViewHolder.bindView(items[position])
    }

    inner class Holder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindView(item: ReportGroupedItem) {
            view.tv_company_name.text = item.company

            if (isAdapterForTaxAgent()) {
                setHolderViewClickListener(item)
            } else {
                view.tv_company_name.gravity = Gravity.CENTER
                view.iv_arrow.setVisible(false)
                view.expl_reports.expand()
                insertReportsInView(item)
                notifyListener(item, true)
            }
        }

        private fun setHolderViewClickListener(item: ReportGroupedItem) {
            view.ll_report_title.setOnClickListener {
                val expandableLayout = view.expl_reports
                if (expandableLayout != lastExpandView) {
                    collapseLastExpandableLayout()
                }

                expandableLayout.toggle()
                if (expandableLayout.isExpanded) {
                    insertReportsInView(item)
                }
                notifyListener(item, expandableLayout.isExpanded)
                setLastExpandableLayout(expandableLayout)
            }
        }

        private fun insertReportsInView(item: ReportGroupedItem) {
            removeAllChildViews()
            val reportsGroup = item.reportGroup

            val hasReports = reportsGroup.any { it.reports.isNotEmpty() }

            if (!hasReports) {
                view.ll_all_report_sent.setVisible(true)
            } else {
                view.ll_all_report_sent.setVisible(false)
                reportsGroup.forEach {
                    if (it.reports.isNotEmpty()) {
                        createTitleAndInsetInView(it.name)
                        it.reports.forEach {
                            createReportAndInsetInView(it)
                        }
                    }
                }
            }
        }

        private fun createTitleAndInsetInView(name: String) {
            val titleView = TitleItemView(view.context).getView(name)
            view.ll_reports_holder.addView(titleView)
        }

        private fun removeAllChildViews() {
            view.ll_reports_holder.removeAllViews()
        }

        private fun isAdapterForTaxAgent(): Boolean = changeListener != null

        private fun setLastExpandableLayout(expandableLayout: ExpandableLayout?) {
            lastExpandView = expandableLayout
        }

        private fun collapseLastExpandableLayout() {
            lastExpandView?.setOnExpansionUpdateListener(null)
            lastExpandView?.collapse()
        }

        private fun createReportAndInsetInView(report: Report) {
            val reportView = ReportItemView(view.context).getView(report)
            view.ll_reports_holder.addView(reportView)
        }

        private fun notifyListener(item: ReportGroupedItem, isExpanded: Boolean) {
            val isEmptyReportList = item.reportGroup.isEmpty()
            lastCompanyName = if (isExpanded && !isEmptyReportList) item.company else null
            changeListener?.onViewExpandChange(lastCompanyName)
        }
    }

    inner class ReportItemView(val context: Context) {
        fun getView(report: Report): View {
            val view = LayoutInflater.from(context).inflate(R.layout.report_view, null, false)
            view.tv_id_name_title.text = report.name
            view.tv_description.text = report.description
            view.tv_period_end.text = context.getString(R.string.hand_over_before_format,
                    report.last_day, DateHelper.getCurrentMonth(), DateHelper.getCurrentYear())
            view.iv_icon.visibility = View.GONE
            view.tv_period.visibility = View.GONE
            view.tv_report_status.visibility = View.GONE
            return view
        }
    }

    inner class TitleItemView(val context: Context) {
        fun getView(name: String): View {
            val view = LayoutInflater.from(context).inflate(R.layout.report_title_view, null, false)
            view.tv_supervisor_name.text = name
            return view
        }
    }

    interface ExpandChangeListener {
        fun onViewExpandChange(companyName: String?)
    }
}