package io.onteam.onoi.network

import io.onteam.onoi.objects.CurrencyRates
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import retrofit2.http.GET

/**
 * Класс для работы с курсами валют НБКР.
 */
class ExchangeConnector {

    //Ссылка на НБКР
    private val bankUrl = "http://www.nbkr.kg/"

    /**
     * Описывает функции доступные для работы в НБКР.
     */
    interface IExchangeService {
        /**
         * Возвращает курсы валют на текущий день.
         */
        @GET("XML/daily.xml")
        fun getExchangeRate(): Call<CurrencyRates>
    }

    //Объект Retrofit для работы с системой НБКР.
    val retrofit = Retrofit.Builder()
            .baseUrl(bankUrl)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build()

    /**
     * Возвращает в callback структуру {@code CurrencyRates} с курсом валют на текущий день.
     */
    fun getRate(callback: Callback<CurrencyRates>) {
        retrofit.create(IExchangeService::class.java).getExchangeRate().enqueue(callback)
    }
}