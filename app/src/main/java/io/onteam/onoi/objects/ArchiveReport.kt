package io.onteam.onoi.objects

data class ArchiveReport(var id: Int, var form_id: Int, val abonent_id: Int,
                         var form_identificator: Int, var name: String, var datetime: String,
                         var description: String, var report_status_id: Int?, var period_type: Int)

//report_status_id : 10 - принят  20 - не принят null - в обработке