package io.onteam.onoi.fragments.newdesign


import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.fragments.RegistrationInfoFragment
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.fragment_sms_code.*

class SmsCodeFragment : BaseFragment() {

    private var progress: SpotsDialog? = null

    override fun getLayoutRes(): Int = R.layout.fragment_sms_code

    override fun getWindowSoftInputMode(): Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        progress = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)
        civ_sms.addTextWatcher(getSmsWatcher())
        civ_sms.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            return@OnEditorActionListener if (actionId == EditorInfo.IME_ACTION_DONE) {
                btn_next.performClick()
                true
            } else {
                false
            }
        })

        civ_sms.focus()
        civ_sms.postDelayed({
            Helpers.showKeyboard(requireContext(), civ_sms)
        }, 100)

        btn_next.setOnClickListener { onNextBtnClick() }
        btn_back.setOnClickListener { onBackBtnClick() }
    }

    private fun onBackBtnClick() {
        popFragment()
    }

    private fun onNextBtnClick() {
        val smsCode = civ_sms.getText()
        if (isValidSmsCode(smsCode)) {
            Helpers.hideKeyboard(requireContext(), civ_sms)
            val pinConfirm = createPinConfirmObject(smsCode)
            OnoyConnector.pinEmployeeConfirm(pinConfirm) { onoyException, response ->
                progress?.dismiss()

                if (response != null) {
                    if (!response.approve) {
                        handleException(response.message)
                        return@pinEmployeeConfirm
                    }
                    val innEmployee = UserManager.registeredUser.innOrganisation
                    requestUserInfo(innEmployee)
                }

                if (onoyException != null) {
                    handleException(onoyException.message)
                }
            }
        } else {
            civ_sms.showError(getString(R.string.incorrect_sms_code_message))
        }
    }

    private fun createPinConfirmObject(smsCode: String): PinConfirm {
        val innOrganisation = UserManager.registeredUser.innOrganisation
        val innEmployee = UserManager.registeredUser.innEmployee
        return PinConfirm(smsCode, innOrganisation, innEmployee)
    }

    private fun handleException(message: String) {
        var textMessage = message
        if (textMessage.isEmpty()) {
            textMessage = getString(R.string.incorrect_sms_code_message)
        }
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setMessage(textMessage)
                .setPositiveButton(R.string.close, null)
                .create().show()
    }

    private fun requestUserInfo(inn: String) {
        checkIsUserContain(inn) { exception, userExistCode ->
            exception?.let {
                showAlertMessage(R.string.error, exception.message)
            }

            userExistCode?.let {
                if (handleIsUserExist(it)) {
                    if (UserManager.registeredUser.isCustomer)
                        showCustomerContainError(inn)
                    else
                        signIn(inn)
                } else {
                    replaceFragment(UserTypeFragment())
                }
            }
        }
    }

    private fun signIn(inn: String) {
        val firebaseKey = Preferences.getString(Preferences.Keys.FIREBASE_KEY)
        val signInfo = SignInfo(inn, "0", firebaseKey)
        OnoyConnector.signIn(signInfo) { exception, userResponse ->
            progress?.dismiss()
            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
            }

            if (userResponse != null) {
                if (userResponse.message.isNotEmpty()) {
                    showAlertMessage(R.string.error, userResponse.message)
                    return@signIn
                }

                userResponse.profile?.let {
                    val user = UserManager.registeredUser
                    if (user.isCustomer) {
                        UserManager.addNewChildren(it)
                        activity?.finish()
                    } else {
                        saveForSignIn(inn, "0")
                        UserManager.setCurrentUser(it)
                        startMainActivity()
                    }
                    UserManager.resetRegisterUser()
                }
            }
        }
    }

    private fun startMainActivity() {
        LocalBroadcastManager.getInstance(requireContext())
                .sendBroadcast(Intent(MainActivity.FINISH_ACTION))

        startActivity(MainActivity.getIntent(requireContext()))
        requireActivity().finish()
    }


    private fun saveForSignIn(inn: String, keyword: String) {
        Preferences.setValue(Preferences.Keys.USER_INN, inn)
        Preferences.setValue(Preferences.Keys.USER_KEYWORD, keyword)
    }

    private fun showCustomerContainError(inn: String) {
        showAlertMessage(R.string.error, getString(R.string.user_exist_format, inn))
    }

    private fun handleIsUserExist(code: Int): Boolean {
        val isCustomer = UserManager.registeredUser.isCustomer
        return when (code) {
            UserExistCode.NO_EXIST.code -> false
            UserExistCode.EXIST_WITH_PARENT.code -> isCustomer
            UserExistCode.EXIST_WITHOUT_PARENT.code -> true
            else -> false
        }
    }

    private fun checkIsUserContain(inn: String, callback: (OnoyException?, Int?) -> Unit) {
        val userExistCall = OnoyConnector.isUserExist(inn) { ex, existCode ->
            progress?.dismiss()
            callback(ex, existCode)
        }

        progress?.setOnCancelListener {
            userExistCall.cancel()
        }
        progress?.show()
    }

    /**
     * Запрашивает у сервера налоговой информацию по ИНН.
     */
    private fun requestInfoByTin(inn: String) {

        val userInfoCall = OnoyConnector.getUserInfo(inn) { exception, user ->
            progress?.dismiss()
            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
                return@getUserInfo
            }

            if (user != null) {
                if (user.Status != UserStatuses.ok) {
                    handleIncorrectStatus(inn, user.Status)
                    return@getUserInfo
                }

                handleResponse(user)
                return@getUserInfo
            }

            showAlertMessage(R.string.error, getString(R.string.bad_server_response_message))
        }
        progress?.show()
        progress?.setOnCancelListener {
            userInfoCall.cancel()
        }
    }

    private fun handleIncorrectStatus(inn: String, status: Int) {
        val message = when (status) {
            UserStatuses.notFound -> getString(R.string.tin_not_found_format, inn)
            UserStatuses.searchError -> getString(R.string.searching_error_message)
            else -> getString(R.string.not_specified_error_format, status)
        }
        showAlertMessage(R.string.error, message)
    }

    private fun handleResponse(user: RegisteredUser) {
        setRegisterUser(user)
        val targetFragment = if (user.tax_agent) {
            TaxAgentRegisterInfoFragment()
        } else {
            RegistrationInfoFragment()
        }
        replaceFragment(targetFragment, true)
    }

    private fun setRegisterUser(user: RegisteredUser) {
        user.tax_agent = UserManager.registeredUser.tax_agent
        user.tax_agent = UserManager.registeredUser.tax_agent
        user.isCustomer = UserManager.registeredUser.isCustomer

        UserManager.registeredUser = user
    }

    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    private fun isValidSmsCode(smsCode: String) = smsCode.length == 4

    private fun getSmsWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                civ_sms.hideError()

                val length = s?.length ?: 0
                if (length > 4) {
                    civ_sms.setText(s?.substring(0, 4) ?: "")
                    civ_sms.moveCursorToEnd()
                }
            }

        }
    }
}
