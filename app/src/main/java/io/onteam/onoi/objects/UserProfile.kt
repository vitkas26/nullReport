package io.onteam.onoi.objects

/**
 * Created by sedi_user on 30.03.2018.
 */
class UserProfile {
    var id: Int = 0
    var inn: String = ""
    var name: String = ""
    var chief: String = ""
    var okpo: Int = 0
    var certificate_image: String = ""
    var passport_image_front: String = ""
    var passport_image_back: String = ""
    var sf_photo: String = ""
    var phone: String? = ""
    var token: String = ""
    var balance: Double = 0.0
    var invited_by: String? = null
    var auto_sent: Boolean = false
    var tax_agent: Boolean = false
    var children: MutableList<UserProfile>? = mutableListOf()
    var parent: String? = null

    fun hasCustomer(): Boolean {
        return children?.isEmpty() == false
    }

    fun addChildren(children: UserProfile) {
        this.children?.add(children)
    }

    fun removeChildren(inn: String) {
        children?.removeAll { profile -> profile.inn == inn }
    }

}