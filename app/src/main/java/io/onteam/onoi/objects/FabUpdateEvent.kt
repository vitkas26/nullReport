package io.onteam.onoi.objects

class FabUpdateEvent (val drawRes: Int, val type: FabUpdateEvent.Type, val action: Runnable?) {
    enum class Type {
        Invitation,
        SendReport,
        SendArchive,
        CustomerRegistration
    }
}