package io.onteam.onoi.fragments.newdesign


import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.fragment_pin_input.*


class PincodeInputFragment : BaseFragment() {

    private var progress: SpotsDialog? = null

    override fun getWindowSoftInputMode(): Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    override fun getLayoutRes(): Int = R.layout.fragment_pin_input

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progress = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)
        pv_code.typeface = Typeface.defaultFromStyle(Typeface.BOLD)

        pv_code.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                btn_next.isEnabled = s?.length!! >= 4
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        btn_next.setOnClickListener { onNextBtnClick() }
    }

    private fun onNextBtnClick() {
        val pinCode = pv_code.text.toString()

        Helpers.hideKeyboard(requireContext(), pv_code)
        val pinConfirm = createPinConfirmObject(pinCode)
        progress?.show()
        OnoyConnector.pinConfirm(pinConfirm) { onoyException, response ->
            progress?.dismiss()

            if (response != null) {
                if (!response.approve) {
                    handleException(response.message)
                    return@pinConfirm
                }
                val innEmployee = UserManager.registeredUser.innOrganisation
                requestUserInfo(innEmployee)
            }

            if (onoyException != null) {
                handleException(onoyException.message)
            }
        }

    }

    private fun createPinConfirmObject(pinCode: String): PinConfirm {
        val innOrganisation = UserManager.registeredUser.innOrganisation
        return PinConfirm(pinCode, innOrganisation, null)
    }

    private fun handleException(message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(R.string.close, null)
                .create().show()
    }

    private fun requestUserInfo(inn: String) {
        checkIsUserContain(inn) { exception, userExistCode ->
            exception?.let {
                showAlertMessage(R.string.error, exception.message)
            }

            userExistCode?.let {
                if (handleIsUserExist(it)) {
                    if (UserManager.registeredUser.isCustomer)
                        showCustomerContainError(inn)
                    else
                        signIn(inn)
                } else {
                    replaceFragment(UserTypeFragment())
                }
            }
        }
    }

    private fun showCustomerContainError(inn: String) {
        showAlertMessage(R.string.error, getString(R.string.user_exist_format, inn))
    }

    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    private fun handleIsUserExist(code: Int): Boolean {
        val isCustomer = UserManager.registeredUser.isCustomer
        return when (code) {
            UserExistCode.NO_EXIST.code -> false
            UserExistCode.EXIST_WITH_PARENT.code -> isCustomer
            UserExistCode.EXIST_WITHOUT_PARENT.code -> true
            else -> false
        }
    }

    private fun checkIsUserContain(inn: String, callback: (OnoyException?, Int?) -> Unit) {
        val userExistCall = OnoyConnector.isUserExist(inn) { ex, existCode ->
            progress?.dismiss()
            callback(ex, existCode)
        }

        progress?.setOnCancelListener {
            userExistCall.cancel()
        }
        progress?.show()
    }

    private fun signIn(inn: String) {
        val firebaseKey = Preferences.getString(Preferences.Keys.FIREBASE_KEY)
        val signInfo = SignInfo(inn, "0", firebaseKey)
        OnoyConnector.signIn(signInfo) { exception, userResponse ->
            progress?.dismiss()
            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
            }

            if (userResponse != null) {
                val userProfile = userResponse.profile as UserProfile

                val user = UserManager.registeredUser
                if (user.isCustomer) {
                    UserManager.addNewChildren(userProfile)
                    activity?.finish()
                } else {
                    saveForSignIn(inn, "0")
                    UserManager.setCurrentUser(userProfile)
                    startMainActivity()
                }
                UserManager.resetRegisterUser()
            }
        }
    }

    private fun startMainActivity() {
        LocalBroadcastManager.getInstance(requireContext())
                .sendBroadcast(Intent(MainActivity.FINISH_ACTION))

        startActivity(MainActivity.getIntent(requireContext()))
        requireActivity().finish()
    }


    private fun saveForSignIn(inn: String, keyword: String) {
        Preferences.setValue(Preferences.Keys.USER_INN, inn)
        Preferences.setValue(Preferences.Keys.USER_KEYWORD, keyword)
    }
}
