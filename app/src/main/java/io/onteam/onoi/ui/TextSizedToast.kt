package io.onteam.onoi.ui

import android.content.Context
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

class TextSizedToast(private val context: Context, private val textSize: Float = 14f) {

    fun show(message: String, duration: Int = Toast.LENGTH_SHORT){
        val toast = Toast.makeText(context, message, duration)
        setTextSize(toast)
        toast.show()
    }

    fun show(messageRes: Int, duration: Int = Toast.LENGTH_SHORT){
        val toast = Toast.makeText(context, messageRes, duration)
        setTextSize(toast)
        toast.show()
    }

    private fun setTextSize(toast: Toast) {
        try {
            val textView = (toast.view as ViewGroup).getChildAt(0) as TextView
            textView.textSize = textSize
        } catch (e: Exception) {
        }
    }

}