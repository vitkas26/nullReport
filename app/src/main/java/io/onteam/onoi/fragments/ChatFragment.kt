package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.MessageManager
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.activities.StartFragmentActivity
import io.onteam.onoi.objects.ChatMessage
import io.onteam.onoi.objects.OnoyConnector
import io.onteam.onoi.objects.StartFragments
import io.onteam.onoi.objects.UserManager
import io.onteam.onoi.ui.TextSizedToast
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_chat.view.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar




class ChatFragment : Fragment(), FragmentVisibleListener {

    private val layoutRes = R.layout.fragment_chat
    private var unregistrar: Unregistrar? = null

    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            scrollRecyclerToBottom()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        initViews(view)
        if (UserManager.isValidCurrentUser())
            MessageManager.requestTop100Message(UserManager.getCurrentUser()!!.id)
        return view
    }

    override fun onDestroyView() {
        unregistrar?.unregister()
        rv_chat.adapter?.unregisterAdapterDataObserver(adapterDataObserver)
        rv_chat.adapter = null

        super.onDestroyView()
    }

    private fun initViews(view: View) {
        view.ibtn_send.setOnClickListener {
            sendMessageClickButton()
        }

        val linearLayoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, true)
        view.rv_chat.layoutManager = linearLayoutManager
        setRecyclerViewAdapter(view)

        unregistrar = KeyboardVisibilityEvent.registerEventListener(requireActivity()) {
            scrollRecyclerToBottom()
        }
    }

    private fun setRecyclerViewAdapter(view: View) {
        val messageAdapter = MessageManager.messageAdapter
        messageAdapter.registerAdapterDataObserver(adapterDataObserver)
        view.rv_chat.adapter = messageAdapter
    }

    private fun sendMessageClickButton() {
        if (!UserManager.isValidCurrentUser()) {
            startActivity(StartFragmentActivity.getIntent(requireContext(), StartFragments.ChooseActionFragment))
        } else {
            val message = getTextMessage()
            if (!message.isEmpty()) {
                setSendButtonEnabled(false)
                val post = createPost(message)
                sendMessage(post) { msg ->
                    savePost(msg)
                    et_message.setText("")
                    setSendButtonEnabled(true)
                }
            }
        }
    }

    private fun savePost(post: ChatMessage) {
        MessageManager.addMessageWithNotification(post)
    }

    private fun createPost(message: String): ChatMessage {
        val inn = io.onteam.onoi.objects.UserManager.getCurrentUser()?.inn!!
        return ChatMessage(inn, message, true)
    }

    private fun sendMessage(message: ChatMessage, callback: ((ChatMessage) -> Unit)) {
        OnoyConnector.sendMessage(message) { exception, success ->
            setSendButtonEnabled(true)

            if (exception != null) {
                TextSizedToast(requireContext()).show(exception.message)
            }

            if (success) {
                callback(message)
            }
        }
    }

    private fun setSendButtonEnabled(isEnabled: Boolean) {
        ibtn_send.isEnabled = isEnabled
    }

    private fun getTextMessage(): String {
        return et_message.text.toString()
    }

    override fun setVisible() {
        (activity as? MainActivity)?.updateFab(null)
        scrollRecyclerToBottom()
    }

    private fun scrollRecyclerToBottom() {
        rv_chat.scrollToPosition(0)
    }

    override fun setUnvisible() {

    }



}
