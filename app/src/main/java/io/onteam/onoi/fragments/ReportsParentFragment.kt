package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.OnReportListChangeListener
import io.onteam.onoi.R
import io.onteam.onoi.objects.ReportsFragmentFabric
import io.onteam.onoi.objects.UserManager
import kotlinx.android.synthetic.main.fragment_reports.*
import kotlinx.android.synthetic.main.fragment_reports.view.*


/**
 * Родительский фрагмет который включает в себя {@code ReportsListFragment} и
 * {@code ArchiveReportsListFragment}, а также табы для переключения между ними.
 */
class ReportsParentFragment : Fragment(), OnReportListChangeListener, FragmentVisibleListener {

    private val layoutRes = R.layout.fragment_reports

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        createViewPagerAndTabs(view)
        return view
    }

    override fun reloadData() {
        val adapter = view_pager.adapter as ReportsPageAdapter
        adapter.fragments.forEach {
            (it as? OnReportListChangeListener)?.reloadData()
        }
        moveToArchiveTab()
    }


    override fun setVisible() {
        if (view_pager != null) {
            val item = (view_pager.adapter as ReportsPageAdapter).getItem(view_pager.currentItem)
            (item as? FragmentVisibleListener)?.setVisible()
        }
    }

    override fun setUnvisible() {
        if (view_pager != null) {
            val item = (view_pager.adapter as ReportsPageAdapter).getItem(view_pager.currentItem)
            (item as? FragmentVisibleListener)?.setUnvisible()
        }
    }

    private fun moveToArchiveTab() {
        tab_layout.getTabAt(1)?.select()
    }

    /**
     * Создает и устанавливает TAB'ы.
     * @param view родительский View.
     */
    private fun createViewPagerAndTabs(view: View) {
        val pageAdapter = createReportsPageAdapter()
        view.view_pager.adapter = pageAdapter
        view.view_pager.offscreenPageLimit = pageAdapter.count
        view.tab_layout.setupWithViewPager(view.view_pager)
        view.tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val item = (view_pager.adapter as ReportsPageAdapter).getItem(tab?.position ?: 0)
                (item as? FragmentVisibleListener)?.setUnvisible()
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val item = (view_pager.adapter as ReportsPageAdapter).getItem(tab?.position ?: 0)
                (item as? FragmentVisibleListener)?.setVisible()
            }

        })
    }

    private fun createReportsPageAdapter(): ReportsPageAdapter {
        val pageAdapter = ReportsPageAdapter(requireFragmentManager())

        val isTaxAgent = isTaxAgent()

        val reportFragment = ReportsFragmentFabric.getReportFragment(isTaxAgent)
        reportFragment.changeListener = this
        pageAdapter.add(reportFragment, "ОТЧЕТЫ")

        val archiveFragment = ReportsFragmentFabric.getArchiveFragment(isTaxAgent)
        pageAdapter.add(archiveFragment, "АРХИВ")
        return pageAdapter
    }

    private fun isTaxAgent(): Boolean{
        return UserManager.isValidCurrentUser() && UserManager.getCurrentUser()!!.tax_agent
    }

    /**
     * Описывает поведение customerAdapter'a для {@link TabLayout}
     */
    class ReportsPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        val fragments = mutableListOf<Fragment>()

        private val titles = mutableListOf<String>()

        /**
         * Добавляет новый Tab.
         * @param fragment фрагмент.
         * @param title заголовок для Tab'a.
         */
        fun add(fragment: Fragment, title: String) {
            fragments.add(fragment)
            titles.add(title)
        }

        /**
         * Возвращает фрагмент для указанной позиции.
         * @param position позиция выбранного  Tab'a.
         *
         * @return fragment для данного позиции Tab'a.
         */
        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        /**
         * Возвращает число элементов в TabBar'e.
         */
        override fun getCount(): Int {
            return fragments.size
        }

        /**
         * Возвращает заголово Tab'a для указанной позиции.
         * @param position позиция выбранного  Tab'a.
         *
         * @return заголовок для данного позиции Tab'a.
         */
        override fun getPageTitle(position: Int): CharSequence {
            return titles[position]
        }
    }
}
