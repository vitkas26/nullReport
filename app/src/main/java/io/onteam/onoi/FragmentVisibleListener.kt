package io.onteam.onoi

interface FragmentVisibleListener {
    fun setVisible()
    fun setUnvisible()
}