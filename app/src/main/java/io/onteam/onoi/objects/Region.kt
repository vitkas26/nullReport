package io.onteam.onoi.objects

data class Region(val id: Int, val codeGni: String?, val codeSf: String?, val name: String?) {
    override fun toString(): String {
        val code = codeGni ?: ""
        val regionName = name ?: ""
        return "($code) $regionName"
    }
}