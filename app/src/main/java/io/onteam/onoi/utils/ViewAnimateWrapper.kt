package io.onteam.onoi.utils

import android.view.View
import android.widget.LinearLayout

class ViewAnimateWrapper(var view: View) {

    fun setWeight(weight: Float) {
        val layoutParams = view.layoutParams as? LinearLayout.LayoutParams
        if (layoutParams != null) {
            layoutParams.weight = weight
            view.layoutParams = layoutParams
            view.parent.requestLayout()
        }

    }

    fun getWeight(): Float {
        val layoutParams = view.layoutParams as? LinearLayout.LayoutParams
        return layoutParams?.weight ?: 1f
    }
}