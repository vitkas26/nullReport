package io.onteam.onoi.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import io.onteam.onoi.R
import io.onteam.onoi.objects.UserProfile
import kotlinx.android.synthetic.main.item_customer_info.view.*


class CustomerAdapter(var customers: MutableList<UserProfile>)
    : RecyclerView.Adapter<CustomerAdapter.CustomerAdapterHolder>() {

    private var lastItem: CustomerAdapter.CustomerAdapterHolder? = null
    var changeListener: OnCustomerChangeListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerAdapterHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_customer_info, parent, false)
        return CustomerAdapterHolder(view)
    }

    override fun getItemCount(): Int = customers.size

    override fun onBindViewHolder(holder: CustomerAdapterHolder, position: Int) {
        holder.bindView(customers[position])
    }

    //region Inner classes
    inner class CustomerAdapterHolder(var view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(userProfile: UserProfile) {

            view.tv_company.text = userProfile.name
            view.civ_director.setText(userProfile.chief)
            view.civ_inn.setText(userProfile.inn)

            view.cb_auto_sent.isChecked = userProfile.auto_sent
            view.cb_auto_sent.setOnCheckedChangeListener(getCheckedChangeListener(userProfile))

            view.btn_remove_customer.setOnClickListener {
                changeListener?.onRemove(userProfile.inn) {
                    if (it) notifyDataSetChanged()
                }
            }

            view.tv_company.setOnClickListener {
                if (this != lastItem) {
                    lastItem?.changeInfoViewExpandState()
                    lastItem?.updateTopArrow()
                }
                view.expl_info.toggle()
                updateTopArrow()
                lastItem = if (view.expl_info.isExpanded) this else null
            }
        }

        private fun updateTopArrow() {
            val rotationValue = if (view.expl_info.isExpanded) 90f else 0f
            animateArrowRotation(view.iv_top_arrow, rotationValue)
        }

        private fun changeInfoViewExpandState() {
            view.expl_info.toggle()
        }

        private fun getCheckedChangeListener(userProfile: UserProfile): (CompoundButton, Boolean) -> Unit {
            return { _, isChecked ->
                changeListener?.onChangeAutoSend(userProfile.inn, isChecked) {

                    view.cb_auto_sent.setOnCheckedChangeListener(null)
                    view.cb_auto_sent.isChecked = if(it) isChecked else !isChecked
                    view.cb_auto_sent.setOnCheckedChangeListener(getCheckedChangeListener(userProfile))
                }
            }
        }

        private fun animateArrowRotation(view: View, rotationValue: Float) {
            val rotation = view.animate().rotation(rotationValue)
            rotation.duration = 300
            rotation.start()
        }

    }
    //endregion

    //region Interfaces
    interface OnCustomerChangeListener {
        fun onRemove(inn: String, callback: (Boolean) -> Unit)
        fun onChangeAutoSend(inn: String, isEnabled: Boolean, callback: (Boolean) -> Unit)
    }
    //endregion
}