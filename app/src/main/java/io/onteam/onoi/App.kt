package io.onteam.onoi

import android.app.Application
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import io.onteam.onoi.utils.NotificationHelper
import io.onteam.onoi.utils.Preferences
import io.paperdb.Paper

/**
 * Created by RAM on 26.03.2018.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        NotificationHelper.init(this)
        Preferences.init(this)
        Paper.init(this)
        Fabric.with(this, Crashlytics())
        io.onteam.onoi.objects.UserManager.requestRegion()
    }
}