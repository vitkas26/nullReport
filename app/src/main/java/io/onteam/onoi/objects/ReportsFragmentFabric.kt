package io.onteam.onoi.objects

import android.support.v4.app.Fragment
import io.onteam.onoi.fragments.ArchiveReportsListFragment
import io.onteam.onoi.fragments.ReportsListFragment
import io.onteam.onoi.fragments.TaxAgentArchiveReportsListFragment
import io.onteam.onoi.fragments.TaxAgentReportsListFragment

object ReportsFragmentFabric {
    fun getReportFragment(isTaxAgent: Boolean): BaseReportFragment {
        return if (isTaxAgent) {
            TaxAgentReportsListFragment()
        } else {
            ReportsListFragment()
        }
    }

    fun getArchiveFragment(isTaxAgent: Boolean): Fragment {
        return if (isTaxAgent) {
            TaxAgentArchiveReportsListFragment()
        } else {
            ArchiveReportsListFragment()
        }
    }


}