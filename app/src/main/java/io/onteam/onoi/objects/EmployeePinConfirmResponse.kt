package io.onteam.onoi.objects

import io.onteam.onoi.utils.empty

class EmployeePinConfirmResponse : BaseResponse() {
    var approve: Boolean = false
    var innOrganisation: String = String.empty()
    var innEmployee: String = String.empty()
    var token: String = String.empty()
}