package io.onteam.onoi.activities

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import io.onteam.onoi.base.BaseFragmentActivity
import io.onteam.onoi.fragments.newdesign.InnCheckFragment
import io.onteam.onoi.objects.UserManager

class CustomerRegistrationActivity : BaseFragmentActivity() {

    companion object {
        fun getIntent(context: Context): Intent {
            return Intent(context, CustomerRegistrationActivity::class.java)
        }
    }

    override fun getFragment(): Fragment {
        UserManager.resetRegisterUser()
        return InnCheckFragment.getInstance(true)
    }
}
