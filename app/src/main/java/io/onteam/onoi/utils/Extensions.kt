package io.onteam.onoi.utils

import android.view.View
import android.widget.LinearLayout

fun View.isVisible(): Boolean {
    return visibility == View.VISIBLE
}

fun View.setVisible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.setWeight(weight: Float) {
    val lp = layoutParams as? LinearLayout.LayoutParams
    if(lp != null){
        lp.weight = weight
    }
    layoutParams = lp
    parent.requestLayout()
}

fun String.Companion.empty() : String = ""



