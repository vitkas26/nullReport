package io.onteam.onoi.objects

enum class UserExistCode(val code: Int) {
    NO_EXIST(0),
    EXIST_WITH_PARENT(2),
    EXIST_WITHOUT_PARENT(1)
}