package io.onteam.onoi.utils

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import io.onteam.onoi.R
import io.onteam.onoi.activities.StartFragmentActivity
import java.util.*

@SuppressLint("StaticFieldLeak")
object NotificationHelper {

    private val defaultChannel = "defaultChannel"
    private var manager: NotificationManager? = null
    private var context: Context? = null

    fun init(context: Context) {
        this.context = context
        manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        prepareChanel(manager!!, defaultChannel)
    }

    fun showChatMessageNotifiction(message: String) {
        val title = context!!.getString(R.string.new_message)
        showNotification(title, message)
    }

    fun showNotification(title: String, content: String) {
        var icon = R.drawable.notification_icon
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            icon = R.mipmap.icon
        }

        val bigTextStyle = NotificationCompat.BigTextStyle()
                .bigText(content)
                .setBigContentTitle(title)

        val id = Random().nextInt().hashCode()

        val notification = NotificationCompat.Builder(context!!, defaultChannel)
                .setContentIntent(createAppPendingIntent(id))
                .setContentTitle(title)
                .setContentText(content)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(icon)
                .setStyle(bigTextStyle)
                .build()

        manager?.notify(id, notification)
    }

    private fun createAppPendingIntent(id: Int): PendingIntent? {
        val intent = StartFragmentActivity.getIntent(context!!, notificationId = id)
        return PendingIntent.getActivity(context!!, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun prepareChanel(manager: NotificationManager, chanelId: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = manager.getNotificationChannel(chanelId)
            if (channel == null) {
                channel = NotificationChannel(chanelId,
                        "Default",
                        NotificationManager.IMPORTANCE_HIGH)
                manager.createNotificationChannel(channel)
            }
        }
    }

    fun playNotificationRingtone() {
        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val ringtone = RingtoneManager.getRingtone(context!!, ringtoneUri)
        ringtone.play()
    }

    fun showBalanceChangeNotifiction(desc: String, diffSum: Int) {
        val title = context!!.getString(R.string.balance_change)

        val content = if (diffSum > 0)
            context!!.getString(R.string.account_replenishment_format, diffSum, desc)
        else
            context!!.getString(R.string.debiting_account_format, diffSum, desc)

        showNotification(title, content)
    }

    fun clearNotification(id: Int) {
        manager?.cancel(id)
    }
}