package io.onteam.onoi.objects

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Default
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false)
class Currency {
    @get:Element(name = "Value")
    @set:Element(name = "Value")
    var Value: String = "0.0"
    set(value) {
        field = value.replace(",", ".")
    }

    @get:Attribute(name = "ISOCode")
    @set:Attribute(name = "ISOCode")
    var ISOCode: String = ""
}