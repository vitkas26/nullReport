package io.onteam.onoi.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object Preferences {

    enum class Keys {
        CURRENCY_EXCHANGE,
        USER_INN,
        USER_KEYWORD,
        FIREBASE_KEY,
        SHOW_INTRO,
        DESUBSCRIBE_IS_SHOWN
    }

    private var sharedPreference: SharedPreferences? = null

    fun init(context: Context) {
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setValue(keys: Keys, value: Any){
        sharedPreference?.edit()?.putString(keys.name, value.toString())?.apply()
    }

    fun getString(keys: Keys): String {
        return sharedPreference?.getString(keys.name, "") ?: ""
    }

    fun getBool(keys: Keys): Boolean{
        val value = getString(keys)
        return value.toBoolean()
    }




}