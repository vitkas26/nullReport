package io.onteam.onoi.objects

import java.lang.Exception

class OnoyException(override var message: String, var isNetworkException: Boolean = false) : Exception()