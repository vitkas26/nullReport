package io.onteam.onoi.objects

/**
 * Created by sedi_user on 29.03.2018.
 */
open class BaseResponse {
    var success: Boolean = false
    var message: String = ""
}