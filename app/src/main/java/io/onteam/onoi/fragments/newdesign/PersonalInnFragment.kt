package io.onteam.onoi.fragments.newdesign


import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.objects.AuthNexts
import io.onteam.onoi.objects.OnoyConnector
import io.onteam.onoi.objects.UserManager
import io.onteam.onoi.utils.Helpers
import kotlinx.android.synthetic.main.fragment_personal_inn.*


class PersonalInnFragment : BaseFragment() {

    private var progress: SpotsDialog? = null

    override fun getLayoutRes(): Int = R.layout.fragment_personal_inn

    override fun getWindowSoftInputMode(): Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        progress = SpotsDialog(requireContext(), R.style.CustomSpotsDialog)
        prepareInnView()
        btn_next.setOnClickListener { onNextBtnClick() }
    }

    private fun prepareInnView() {
        et_inn.addTextChangedListener(getInnWatcher())
        et_inn.setOnEditorActionListener(TextView.OnEditorActionListener { v, _, _ ->
            val length = v?.text?.length ?: 0
            if (length < 14) {
                Toast.makeText(requireContext(), R.string.incorrect_inn_lenght_message,
                        Toast.LENGTH_SHORT).show()
                return@OnEditorActionListener true
            }
            true
        })
    }

    private fun onNextBtnClick() {
        val inn = et_inn.text.toString()
        if (inn.isEmpty() || inn.length < 14) {
            Toast.makeText(requireContext(), getString(R.string.incorrect_inn_lenght_message), Toast.LENGTH_LONG).show()
            et_inn.requestFocus()
            return
        }

        Helpers.hideKeyboard(requireContext(), et_inn)
        signIn(inn)
    }

    private fun signIn(innEmployee: String) {
        val innOrganisation = UserManager.registeredUser.innOrganisation

        val signIn = OnoyConnector.employeeCheck(innOrganisation, innEmployee) { onoyException, response ->
            progress?.dismiss()

            if (response != null) {
                if (!response.success) {
                    handleException(response.message)
                    return@employeeCheck
                }
                handleResponse(innEmployee)
            }

            if (onoyException != null) {
                handleException(onoyException.message)
            }
        }
        progress?.setOnCancelListener { signIn.cancel() }
        progress?.show()
    }

    private fun handleResponse(innEmployee: String) {
        UserManager.registeredUser.innEmployee = innEmployee
        replaceFragment(SmsCodeFragment(), true)
    }


    private fun handleException(message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(R.string.close, null)
                .create().show()
    }

    private fun getInnWatcher(): TextWatcher {
        val inputTinLimit = 14
        return object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

                btn_next.isEnabled = (p0 != null && p0.length == inputTinLimit)

                if (p0 != null && p0.length > inputTinLimit) {
                    et_inn.setText(p0.substring(0, inputTinLimit))
                    et_inn.setSelection(et_inn.text!!.length)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //et_inn.hideError()
            }
        }
    }


}
