package io.onteam.onoi.ui

import android.content.Context
import android.content.res.ColorStateList
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import io.onteam.onoi.R
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.view_custom_input.view.*


class CustomInputView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : LinearLayout(context, attrs, defStyleAttr) {

    private var showErrorNow = false
    private var focusChangeListener: OnFocusChangeListener? = null

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?) : this(context, null, 0)

    init {

        val typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomInputView_Base, 0, 0)

        val titleRes = typedArray.getResourceId(R.styleable.CustomInputView_Base_titleText, 0)
        val hintRes = typedArray.getResourceId(R.styleable.CustomInputView_Base_hintText, 0)
        val imeOption = typedArray.getInteger(R.styleable.CustomInputView_Base_android_imeOptions, -1)
        val inputType = typedArray.getInteger(R.styleable.CustomInputView_Base_android_inputType, -1)
        val textSize = typedArray.getDimensionPixelSize(R.styleable.CustomInputView_Base_titleTextSize, 0)
        val isPassword = typedArray.getBoolean(R.styleable.CustomInputView_Base_isPassword, false)
        val clickable = typedArray.getBoolean(R.styleable.CustomInputView_Base_android_clickable, true)
        val focusable = typedArray.getBoolean(R.styleable.CustomInputView_Base_android_focusable, true)
        val focusableInTouchMode = typedArray.getBoolean(R.styleable.CustomInputView_Base_android_focusableInTouchMode, true)

        typedArray.recycle()

        val inflater = getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.view_custom_input, this, true)

        with(view) {
            tv_title.setVisible(titleRes > 0)

            if (titleRes > 0)
                tv_title.setText(titleRes)

            editText.hint = if (hintRes > 0) getContext().getString(hintRes) else ""
            if (inputType > 0)
                editText.inputType = inputType

            if (isPassword) {
                editText.setSelection(editText.text!!.length)
                til_custom.isPasswordVisibilityToggleEnabled = true
            }

            if (imeOption > 0)
                editText.imeOptions = imeOption

            editText.isFocusable = focusable
            editText.isClickable = clickable
            if (!clickable)
                editText.inputType = InputType.TYPE_NULL

            editText.isFocusableInTouchMode = focusableInTouchMode

            editText.setOnFocusChangeListener { v, hasFocus ->

                focusChangeListener?.onFocusChange(v, hasFocus)

                val colorRes = when {
                    showErrorNow -> R.color.red_700
                    hasFocus -> R.color.colorAccent
                    else -> R.color.lite_gray
                }
                val color = ContextCompat.getColor(context!!, colorRes)
                tv_title.setTextColor(color)
                editText.supportBackgroundTintList = ColorStateList.valueOf(color)
            }

            if (textSize > 0)
                tv_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())

        }
        refreshDrawableState()
    }

    fun focus(): Boolean {
        return editText.requestFocus()
    }

    fun setOnEditorActionListener(listener: TextView.OnEditorActionListener) {
        editText.setOnEditorActionListener(listener)
    }

    fun getText(): String = editText.text.toString()

    fun setText(text: CharSequence) {
        editText.setText(text)
    }

    fun showError(error: String) {
        showErrorNow = true
        ll_error.visibility = View.VISIBLE
        tv_error.text = error
        val color = ContextCompat.getColor(context!!, R.color.red_700)
        tv_title.setTextColor(color)
        editText.supportBackgroundTintList = ColorStateList.valueOf(color)
    }

    fun hideError() {
        showErrorNow = false
        ll_error.visibility = View.INVISIBLE
        tv_error.text = ""
        val colorRes = if (editText.hasFocus()) R.color.colorAccent else R.color.lite_gray
        val color = ContextCompat.getColor(context!!, colorRes)
        tv_title.setTextColor(color)
        editText.supportBackgroundTintList = ColorStateList.valueOf(color)
    }

    fun addTextWatcher(tw: TextWatcher) {
        editText.addTextChangedListener(tw)
    }

    fun moveCursorToEnd() {
        editText.setSelection(editText.text!!.length)
    }

    fun getEditText(): EditText = editText

    override fun setOnFocusChangeListener(listener: OnFocusChangeListener) {
        focusChangeListener = listener
    }


}