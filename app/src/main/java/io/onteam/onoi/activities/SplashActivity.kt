package io.onteam.onoi.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import io.onteam.onoi.R
import io.onteam.onoi.utils.Helpers
import kotlinx.android.synthetic.main.activity_splash.*

/**
 * Activity отвечеющее за экран загрузки приложения.
 */
class SplashActivity : AppCompatActivity() {

    private val layoutRes = R.layout.activity_splash
    //Timeout перед показом кнопок
    private val timeout = 2000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        setVersionName()

        Handler().postDelayed({
            if (!checkNetworkConnection()) return@postDelayed

            startActivity(StartFragmentActivity.getIntent(this))
            finish()
        }, timeout)
    }

    /**
     * Устанавливает номер версии приложения в правый нижний угол.
     */
    private fun setVersionName() {
        val packageInfo = packageManager.getPackageInfo(applicationInfo.packageName, 0)
        tvVersion.text = packageInfo.versionName
    }

    /**
     * Возвращает флаг наличие активного подключения к сети.
     */
    private fun checkNetworkConnection(): Boolean {
        if (!Helpers.checkConnection(this)) {
            showMissingConnectionAlert()
            return false
        }
        return true
    }

    /**
     * Отображает диалог с сообщение о отсутствующем подключении.
     */
    private fun showMissingConnectionAlert() {
        AlertDialog.Builder(this, R.style.CustomAlertDialogTheme)
                .setCancelable(false)
                .setMessage(R.string.missing_connection_message)
                .setPositiveButton(R.string.on) { _, _ ->
                    startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
                }
                .setNegativeButton(R.string.exit, null)
                .create().show()
    }
}


