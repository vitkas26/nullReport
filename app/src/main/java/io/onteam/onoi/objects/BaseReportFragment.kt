package io.onteam.onoi.objects

import android.support.v4.app.Fragment
import io.onteam.onoi.OnReportListChangeListener

open class BaseReportFragment: Fragment() {
    var changeListener: OnReportListChangeListener? = null
}