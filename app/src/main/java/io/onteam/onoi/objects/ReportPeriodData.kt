package io.onteam.onoi.objects

data class ReportPeriodData(val inn: String, val email: String, val dateFrom: String, val dateTo: String)