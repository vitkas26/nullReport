package io.onteam.onoi.firebase.cloud_message

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.firebase.messaging.RemoteMessage
import io.onteam.onoi.MessageManager
import io.onteam.onoi.objects.ChatMessage
import io.onteam.onoi.objects.LocalBroadcatActions
import io.onteam.onoi.utils.NotificationHelper

class RemoteMessageHandler() {

    private val tag = "RemoteMessageHandler"
    private var localBroadcastManager: LocalBroadcastManager? = null

    constructor(localBroadcastManager: LocalBroadcastManager) : this() {
        this.localBroadcastManager = localBroadcastManager
    }

    fun handleMessage(remoteMessage: RemoteMessage) {
        val type = getTypeRemoteMessage(remoteMessage)
        when (type) {
            RemoteMessageType.ChatMessage -> handleChatMessage(remoteMessage)
            RemoteMessageType.BalanceChange -> handleBalanceChangeMessage(remoteMessage)
            RemoteMessageType.CommonNotification -> handleCommonNotificationMessage(remoteMessage)
            else -> Log.w(tag, "Receive unsupported RemoteMessage item.")
        }
    }

    private fun handleCommonNotificationMessage(remoteMessage: RemoteMessage) {
        val title = remoteMessage.data["title"] ?: ""
        val message = remoteMessage.data["message"] ?: ""
        NotificationHelper.showNotification(title, message)
    }

    private fun handleBalanceChangeMessage(remoteMessage: RemoteMessage) {
        val desc = remoteMessage.data["desc"] ?: ""
        val diff = remoteMessage.data["difference"]?.toIntOrNull()
        diff?.let {
            NotificationHelper.showBalanceChangeNotifiction(desc, diff)
            sendBalanceChangeBroadcast(remoteMessage.data["balance"])
        }
    }

    private fun sendBalanceChangeBroadcast(balance: String?) {
        balance?.let {
            it.toDoubleOrNull()?.let {
                val intent = Intent(LocalBroadcatActions.CHANGE_BALANCE)
                intent.putExtra("balance", it)
                sendIntentBroadcast(intent)
            }
        }
    }

    private fun handleChatMessage(remoteMessage: RemoteMessage) {
        val inn = remoteMessage.data["inn"]
        val message = remoteMessage.data["message"]

        if (!inn.isNullOrEmpty() && !message.isNullOrEmpty()) {
            val post = ChatMessage(inn!!, message!!, false)
            MessageManager.addMessageWithNotification(post)
        }
    }

    private fun getTypeRemoteMessage(remoteMessage: RemoteMessage): RemoteMessageType {
        val type = remoteMessage.data["type"]
        return when (type) {
            "ChatMessage" -> RemoteMessageType.ChatMessage
            "BalanceChange" -> RemoteMessageType.BalanceChange
            "CommonNotification" -> RemoteMessageType.CommonNotification
            else -> RemoteMessageType.Unknown
        }
    }

    private fun sendActionBroadcast(action: String) {
        val intent = Intent(action)
        sendIntentBroadcast(intent)
    }

    private fun sendIntentBroadcast(intent: Intent) {
        localBroadcastManager?.apply {
            sendBroadcast(intent)
        }
    }

    enum class RemoteMessageType {
        ChatMessage,
        BalanceChange,
        CommonNotification,
        Unknown
    }
}