package io.onteam.onoi.dialogs

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.objects.OnoyConnector
import io.onteam.onoi.objects.ReportPeriodData
import io.onteam.onoi.objects.UserManager
import io.onteam.onoi.utils.DateHelper
import kotlinx.android.synthetic.main.email_period_dialog.*
import kotlinx.android.synthetic.main.email_period_dialog.view.*
import java.text.SimpleDateFormat
import java.util.*

class EmailPeriodDialog : AppCompatDialogFragment() {
    val layoutRes = R.layout.email_period_dialog

    companion object {
        private val INN = "INN"
        fun getInstance(inn: String): EmailPeriodDialog {
            val bundle = Bundle()
            bundle.putString(INN, inn)
            val periodDialog = EmailPeriodDialog()
            periodDialog.arguments = bundle
            return periodDialog
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(layoutRes, container, false)
        prepareDialog(view)
        return view
    }

    override fun onResume() {
        val attributes = dialog.window.attributes
        attributes.width = ViewGroup.LayoutParams.MATCH_PARENT
        dialog.window.attributes = attributes
        super.onResume()
    }

    private fun prepareDialog(view: View) {
        view.et_date_from.setText(DateHelper.getDateWithFormat("MM.yyyy"))
        view.et_date_to.setText(DateHelper.getDateWithFormat("MM.yyyy"))

        view.et_date_from.setOnClickListener { showMonthPicker(view.et_date_from) }
        view.et_date_to.setOnClickListener { showMonthPicker(view.et_date_to) }

        view.btn_send.setOnClickListener { sendReports() }
        view.btn_cancel.setOnClickListener { dismiss() }
    }

    private fun showMonthPicker(editText: EditText) {
        val bundle = Bundle()
        bundle.putString("date", editText.text.toString())

        val simpleMonthPicker = SimpleMonthPickerDialog()
        simpleMonthPicker.arguments = bundle
        simpleMonthPicker.setSaveListiner(object : SimpleMonthPickerDialog.OnSaveListener {
            override fun save(string: String) {
                editText.setText(string)
            }
        })
        simpleMonthPicker.show(fragmentManager, "")
    }

    private fun sendReports() {
        val email = civ_email.getText()
        if (email.isEmpty()) {
            showMessage(getString(R.string.empty_mail_error))
            return
        }

        val from = "01.${et_date_from.text}"
        val to = "30.${et_date_to.text}"

        var sdf = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        val dateFrom = sdf.parse(from)
        val dateTo = sdf.parse(to)

        if (dateFrom.after(dateTo)) {
            showMessage(getString(R.string.invalid_range_error))
            return
        }

        sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        val progressDialog = SpotsDialog(context, R.style.CustomSpotsDialog)
        val periodData = ReportPeriodData(getInn(), email, sdf.format(dateFrom), sdf.format(dateTo))
        val pdfCall = OnoyConnector.getPdf(periodData) { onoyException, isSuccess ->
            progressDialog.dismiss()
            if (onoyException != null) {
                showMessage(onoyException.message)
            }

            if (isSuccess) {
                Toast.makeText(context, getString(R.string.report_success_sended_format, email),
                        Toast.LENGTH_SHORT).show()
                dismiss()
            }
        }
        progressDialog.setOnCancelListener { pdfCall.cancel() }
        progressDialog.show()
    }

    private fun getInn(): String {
        return arguments?.getString(INN) ?: UserManager.getCurrentUser()?.inn!!
    }

    private fun showMessage(string: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setTitle(R.string.error)
                .setMessage(string)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, tag)
    }
}