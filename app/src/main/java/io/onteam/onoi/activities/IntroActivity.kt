package io.onteam.onoi.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import io.onteam.onoi.R
import io.onteam.onoi.fragments.IntroFragment
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : AppCompatActivity() {

    private lateinit var adapter: IntroPageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        Preferences.setValue(Preferences.Keys.SHOW_INTRO, true)

        createIntroAdapter()

        viewPager.adapter = adapter
        pageIndicator.releaseViewPager()

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                transformButton(position)
            }

        })

        cl_next.setOnClickListener {
            onNextClick()
        }

        ibtn_close.setOnClickListener { finish() }
    }

    private fun onNextClick() {
        val currentItem = viewPager.currentItem
        val count = adapter.count - 1
        if (currentItem == count) {
            finish()
        } else {
            val newPosition = currentItem + 1
            transformButton(newPosition)
            viewPager.setCurrentItem(newPosition, true)
        }
    }

    private fun transformButton(position: Int) {
        val isLast = (position == adapter.count - 1)

        val textRes = if (isLast) R.string.start_work else R.string.next
        val colorRes = if (isLast) R.color.colorAccent else R.color.lite_gray

        tv_next.setText(textRes)
        cl_next.setBackgroundColor(ContextCompat.getColor(this, colorRes))
    }

    private fun createIntroAdapter(): FragmentStatePagerAdapter {
        adapter = IntroPageAdapter(supportFragmentManager)
        adapter.fragments.add(IntroFragment.getInstance(R.drawable.intro_0, R.string.empty, R.string.intro_message_0))
        adapter.fragments.add(IntroFragment.getInstance(R.drawable.intro_1, R.string.intro_title_1, R.string.intro_message_1))
        adapter.fragments.add(IntroFragment.getInstance(R.drawable.intro_2, R.string.intro_title_2, R.string.intro_message_2))
        adapter.fragments.add(IntroFragment.getInstance(R.drawable.intro_3, R.string.intro_title_3, R.string.intro_message_3))
        return adapter
    }


    inner class IntroPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        val fragments = mutableListOf<Fragment>()

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size

    }

}
