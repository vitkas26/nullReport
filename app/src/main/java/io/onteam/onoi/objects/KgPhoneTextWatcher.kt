package io.onteam.onoi.objects

import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.widget.EditText
import io.onteam.onoi.R

class KgPhoneTextWatcher(private val editText: EditText, private val onTextChange: (() -> Unit)?) : TextWatcher {

    val defaultCode = "+996 "

    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        onTextChange?.invoke()

        var text = p0.toString()
        if (text.length < 5) {
            setTextWithMoveCursor(defaultCode)
        }

        if (p2 < p3) {
            if (text.length == 8 || text.length == 11 || text.length == 14) {
                text += " "
                setTextWithMoveCursor(text)
            }
        }

        if (text.length > 17) {
            text = text.substring(0, 17)
            setTextWithMoveCursor(text)
        }
    }

    private fun setTextWithMoveCursor(text: String) {
        val spannableString = getSpannableCode(text)
        editText.setText(spannableString)
        editText.setSelection(text.length)
    }

    private fun getSpannableCode(text: String): SpannableString {
        val spannableString = SpannableString(text)
        spannableString.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(editText.context, R.color.lite_gray)),
                0,
                defaultCode.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannableString
    }
}