package io.onteam.onoi

interface OnReportListChangeListener {
    fun reloadData()
}