package io.onteam.onoi.fragments


import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import io.onteam.onoi.base.IBaseFragmentActivity
import io.onteam.onoi.objects.*
import io.onteam.onoi.ui.CustomInputView
import io.onteam.onoi.ui.RegionSpinnerAdapter
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.Preferences
import kotlinx.android.synthetic.main.fragment_registration_info.*

/**
 * Fragment используемый для второго шага при регистрации пользователя.
 * В данном Fragment'e пользователь вводит ИНН и ИМЯ ДИРЕКТОРА, а так же КОДОВОЕ СЛОВО.
 */
class RegistrationInfoFragment : BaseFragment() {
    override fun getLayoutRes() = R.layout.fragment_registration_info

    //Число знаков в ИНН
    private val inputTinLimit = 14

    private var progressDialog: Dialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = SpotsDialog(context, R.style.CustomSpotsDialog)
        val newRegistrationUserInfo = UserManager.registeredUser

        requestUserInfo(newRegistrationUserInfo) { user ->
            initRegionSpinner()
            updateViewsInfo(user)
        }
    }

    private fun requestUserInfo(user: RegisteredUser, callback: (RegisteredUser) -> Unit) {
        val userInfo = OnoyConnector.getUserInfo(user.innOrganisation) { onoyException, registeredUser ->

            progressDialog?.dismiss()

            onoyException?.let {
                handleExceptionUserInfo(it, user, callback)
            }

            registeredUser?.let {
                if (it.Status != UserStatuses.ok) {
                    handleInvalidStatus(it.Status)
                    return@getUserInfo
                }
                handleUserInfoRespose(it, user, callback)
            }
        }
        progressDialog?.setOnCancelListener { userInfo.cancel() }
        progressDialog?.show()
    }

    private fun handleInvalidStatus(status: Int) {
        val exMessageRes = when (status) {
            UserStatuses.notFound -> R.string.user_not_found
            UserStatuses.searchError -> R.string.searching_error_message
            else -> R.string.error
        }
        AlertDialog.Builder(requireContext())
                .setMessage(exMessageRes)
                .setPositiveButton(R.string.retry, null)
                .create()
                .show()
    }

    private fun handleUserInfoRespose(newUser: RegisteredUser, oldUser: RegisteredUser, callback: (RegisteredUser) -> Unit) {
        newUser.innEmployee = oldUser.innEmployee
        newUser.innOrganisation = oldUser.innOrganisation
        newUser.isCustomer = oldUser.isCustomer
        newUser.taxRegime = oldUser.taxRegime
        newUser.subjectType = oldUser.subjectType

        UserManager.registeredUser = newUser

        callback(UserManager.registeredUser)

    }

    private fun handleExceptionUserInfo(onoyException: OnoyException, user: RegisteredUser, callback: (RegisteredUser) -> Unit) {
        AlertDialog.Builder(requireContext())
                .setMessage(onoyException.message)
                .setPositiveButton(R.string.retry) { _, _ ->
                    requestUserInfo(user, callback)
                }
                .setNegativeButton(R.string.close) { _, _ ->
                    requireActivity().finish()
                }
                .create()
                .show()
    }

    private fun initRegionSpinner() {
        val regions = UserManager.regions
        val arrayAdapter = RegionSpinnerAdapter(context!!, regions)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnr_region.adapter = arrayAdapter
        val region = regions.firstOrNull { it.codeGni == UserManager.registeredUser.getRegionCode() }
        if (region != null) {
            val position = arrayAdapter.getPosition(region)
            spnr_region.setSelection(position)
        }
    }


    /**
     * Обновляет поля с информацией по пользователю.
     * @param user информация о пользователе.
     */
    private fun updateViewsInfo(user: RegisteredUser) {

        civ_tin.setText(user.innOrganisation)
        civ_comp_name.setText(user.Name)
        civ_address.setText(user.Address)

        civ_director_pin.setText("")
        civ_director_pin.addTextWatcher(getPinChangeListener(civ_director_pin))
        civ_director_name.setText("")
        civ_director_name.addTextWatcher(getNameChangeListener(civ_director_name))

        civ_accountant_pin.setText("")
        civ_accountant_pin.addTextWatcher(getPinChangeListener(civ_accountant_pin))
        civ_accountant_name.setText("")
        civ_accountant_name.addTextWatcher(getNameChangeListener(civ_accountant_name))

        civ_director_pin.setOnFocusChangeListener { _, b ->
            run {
                if (!b) {
                    var message: String? = getString(R.string.incorrect_inn_lenght_message)
                    if (civ_director_pin.getText().length == 14) {
                        message = null
                        Helpers.hideKeyboard(context!!, civ_director_pin)
                        requestInfoByPin(civ_director_pin, civ_director_name)
                    }
                    message?.let { civ_director_pin.showError(it) }
                }
            }
        }

        civ_accountant_pin.setOnFocusChangeListener { _, b ->
            run {
                if (!b) {
                    var message: String? = getString(R.string.incorrect_inn_lenght_message)
                    if (civ_accountant_pin.getText().length == 14) {
                        message = null
                        Helpers.hideKeyboard(context!!, civ_accountant_pin)
                        requestInfoByPin(civ_accountant_pin, civ_accountant_name)
                    }
                    message?.let { civ_accountant_pin.showError(it) }
                }
            }
        }

        btn_next.setOnClickListener {
            if (saveInputValues()) {
                doRegistration(UserManager.registeredUser)
            }
        }

        btn_back.setOnClickListener {
            fragmentManager?.popBackStack()
        }
    }

    /**
     * Производит регистрацию/авторизацию пользователя в системе.
     * Если авторизация успешна - открываем основной экран приложения.
     * @param user информация о пользователе.
     */
    private fun doRegistration(user: RegisteredUser) {
        val registrationCall = OnoyConnector.registration(user) { exception, response ->
            progressDialog?.dismiss()

            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
            }

            if (response != null) {
                if (response.success) {
                    signIn(user.innOrganisation, "0")
                } else {
                    showAlertMessage(R.string.error, response.message)
                }
            }
        }
        progressDialog?.setOnCancelListener { registrationCall.cancel() }
        progressDialog?.show()
    }

    /**
     * Получает информацию по авторизуемому пользователю.
     * @param inn ИНН пользователя.
     * @param keyword кодовое слово.
     */
    private fun signIn(inn: String, keyword: String) {
        val firebaseKey = Preferences.getString(Preferences.Keys.FIREBASE_KEY)
        val signInfo = SignInfo(inn, keyword, firebaseKey)
        OnoyConnector.signIn(signInfo) { exception, userResponse ->
            if (exception != null) {
                showAlertMessage(R.string.error, exception.message)
            }

            if (userResponse != null) {
                saveForSignIn(inn, keyword)
                UserManager.setCurrentUser(userResponse.profile as UserProfile)
                (requireActivity() as? IBaseFragmentActivity)?.replaceFragment(
                        FinishFragment(),
                        false
                )
            }
        }
    }

    private fun saveForSignIn(inn: String, keyword: String) {
        Preferences.setValue(Preferences.Keys.USER_INN, inn)
        Preferences.setValue(Preferences.Keys.USER_KEYWORD, keyword)
    }

    /**
     * Отображает некоторое сообщение как @{code AlertDialog} с текстом @{code message}.
     * @param message сообщение для отображения.
     */
    private fun showAlertMessage(title: Int, message: String) {
        AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    private fun saveInputValues(): Boolean {

        val userInfo = UserManager.registeredUser

        userInfo.region_id = (spnr_region.selectedItem as Region).id

        val directorPin = civ_director_pin.getText()
        if (!directorPin.isEmpty() && directorPin.length == 14) {
            userInfo.chief_pin = directorPin
        } else {
            civ_director_pin.showError(getString(R.string.incorrect_inn_lenght_message))
            return false
        }

        val directorName = civ_director_name.getText()
        if (!directorName.isEmpty()) {
            userInfo.chief = directorName
        } else {
            civ_director_name.showError(getString(R.string.incorrect_field_lenght_message))
            return false
        }

        val accountantPin = civ_accountant_pin.getText()
        if (!accountantPin.isEmpty()) {
            userInfo.accountant_pin = accountantPin
        } else {
            civ_accountant_pin.showError(getString(R.string.incorrect_inn_lenght_message))
            return false
        }

        val accountantName = civ_accountant_name.getText()
        if (!accountantName.isEmpty()) {
            userInfo.accountant_name = accountantName
        } else {
            civ_accountant_name.showError(getString(R.string.incorrect_field_lenght_message))
            return false
        }

        val socialNumber = civ_social_number.getText()
        if (!socialNumber.isEmpty()) {
            userInfo.social_number = socialNumber
        } else {
            civ_social_number.showError(getString(R.string.incorrect_field_lenght_message))
            return false
        }

        return true
    }

    private fun getPinChangeListener(targetView: CustomInputView): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.length == 1) {
                    val last = p0.last()
                    if (!(last == '1' || last == '2')) {
                        targetView.setText("")
                        Toast.makeText(context, getString(R.string.invalid_first_inn_symbol),
                                Toast.LENGTH_SHORT).show()
                        return
                    }
                }

                if (p0.length > inputTinLimit) {
                    targetView.setText(p0.substring(0, inputTinLimit))
                    targetView.moveCursorToEnd()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                targetView.hideError()
            }
        }
    }


    private fun getNameChangeListener(targetView: CustomInputView): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                targetView.hideError()
            }
        }
    }

    /**
     * Запрашивает у сервера информацию по ИНН.
     */
    private fun requestInfoByPin(pinView: CustomInputView, nameView: CustomInputView) {
        val inn = pinView.getText()
        OnoyConnector.getUserInfo(inn) { exception, user ->
            progressDialog?.dismiss()

            if (exception != null) {
                pinView.showError(exception.message)
                return@getUserInfo
            }

            if (user != null) {
                if (user.Status != UserStatuses.ok) {
                    pinView.showError(getIncorrectStatusMessage(inn, user.Status))
                    return@getUserInfo
                }

                handleDirectorInfo(user, nameView)
                return@getUserInfo
            }

            civ_director_name.showError(getString(R.string.null_object_by_tin_format))
        }

        progressDialog?.show()
    }

    private fun handleDirectorInfo(user: RegisteredUser, nameView: CustomInputView) {
        nameView.setText(user.Name)
        nameView.moveCursorToEnd()
        btn_next.requestFocus()
    }

    private fun getIncorrectStatusMessage(inn: String, status: Int): String {
        return when (status) {
            UserStatuses.notFound -> getString(R.string.tin_not_found_format, inn)
            UserStatuses.searchError -> getString(R.string.searching_error_message)
            else -> getString(R.string.not_specified_error_format, status)
        }
    }
}
