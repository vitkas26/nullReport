package io.onteam.onoi.objects

object UserManager {

    //Используется для отвязки от родителя при регистрации.
    const val EDIT = "edit"

    var regions: Array<Region> = emptyArray()
    var registeredUser: RegisteredUser = RegisteredUser()
    private var currentUser: UserProfile? = null
    var changeListeners: MutableList<UserManagerChangeListener> = mutableListOf()

    fun resetRegisterUser() {
        registeredUser = RegisteredUser()
    }

    fun removeChildren(inn: String) {
        currentUser?.removeChildren(inn)
        notifyChangeListener()
    }

    fun addNewChildren(children: UserProfile) {
        if (currentUser?.children == null) {
            currentUser?.children = mutableListOf()
        }

        currentUser?.addChildren(children)
        notifyChangeListener()
    }

    private fun notifyChangeListener() {
        changeListeners.forEach {
            it.currentUserInfoChange()
        }
    }

    fun requestRegion() {
        if (regions.isEmpty()) {
            OnoyConnector.getRegions { regions -> UserManager.regions = regions }
        }
    }

    fun getCurrentUser() = currentUser

    fun setCurrentUser(profile: UserProfile) {
        currentUser = profile
    }

    fun getChildrenByName(companyName: String?): UserProfile? {
        return currentUser?.children?.firstOrNull { it.name == companyName }
    }

    fun isValidCurrentUser(): Boolean = currentUser != null

    fun resetCurrentUser() {
        currentUser = null
    }

    interface UserManagerChangeListener {
        fun currentUserInfoChange()
    }

}