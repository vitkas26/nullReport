package io.onteam.onoi.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.activities.StartFragmentActivity
import io.onteam.onoi.network.ExchangeConnector
import io.onteam.onoi.objects.*
import io.onteam.onoi.ui.NewsAdapter
import io.onteam.onoi.utils.DateHelper
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.Preferences
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.fragment_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainFragment : Fragment(), FragmentVisibleListener {
    private val layoutRes = R.layout.fragment_main
    private val news = mutableListOf<News>()
    private val adapter = NewsAdapter(news)

    private var exchangeRequestOnce: Boolean = false
    private var isVisibleNow = false

    private var balanceChangeReceiver: BroadcastReceiver? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        registerBalanceReceiver()
        requestExchangeRate()
        requestNews()
        updateFabVisibility()
    }

    override fun onDestroyView() {
        unregisterReceiver()
        super.onDestroyView()
    }

    private fun initViews() {
        tv_exchange_rate.text = Preferences.getString(Preferences.Keys.CURRENCY_EXCHANGE)
        tv_current_date.text = DateHelper.getDateWithFormat("dd.MM.yyyy")

        val balance = UserManager.getCurrentUser()?.balance
        updateBalance(balance)


        refresh_layout.setColorSchemeResources(R.color.colorAccent)
        refresh_layout.setOnRefreshListener { requestNews() }

        rv_news.layoutManager = LinearLayoutManager(context)
        rv_news.adapter = adapter
    }

    private fun unregisterReceiver() {
        val broadcastManager = LocalBroadcastManager.getInstance(requireContext())
        try {
            balanceChangeReceiver?.let {
                broadcastManager.unregisterReceiver(it)
            }
        } catch (ignored: Exception) {
        }
    }

    private fun registerBalanceReceiver() {
        balanceChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let {
                    val double = it.getDoubleExtra("balance", 0.0)
                    updateBalance(double)
                }
            }
        }

        LocalBroadcastManager.getInstance(requireContext())
                .registerReceiver(balanceChangeReceiver!!,
                        IntentFilter(LocalBroadcatActions.CHANGE_BALANCE))
    }

    private fun updateBalance(balance: Double?) {
        tv_balance?.apply {
            text = getString(R.string.balance_format, balance).toUpperCase()
            visibility = if (balance != null) View.VISIBLE else View.INVISIBLE
        }
    }

    private fun requestExchangeRate() {
        if (exchangeRequestOnce) return
        tv_exchange_rate.setVisible(false)
        pb_excange.setVisible(true)
        ExchangeConnector().getRate(getExchangeCallback())
        exchangeRequestOnce = true
    }

    private fun getExchangeCallback(): Callback<CurrencyRates> {
        return object : Callback<CurrencyRates> {
            override fun onFailure(call: Call<CurrencyRates>?, t: Throwable?) {
                pb_excange.setVisible(false)
            }

            override fun onResponse(call: Call<CurrencyRates>?, response: Response<CurrencyRates>?) {
                pb_excange.setVisible(false)
                if (response == null || !response.isSuccessful) return
                val rates = response.body() ?: return

                var currency = "НБКР  "
                rates.Currency.forEach {
                    //Нужны курсы валют все кроме - тенге.
                    if (it.ISOCode != "KZT") {
                        currency += "${it.ISOCode} ${it.Value.dropLast(2)}  "
                    }
                }

                Preferences.setValue(Preferences.Keys.CURRENCY_EXCHANGE, currency)
                tv_exchange_rate.text = currency
                tv_exchange_rate.setVisible(true)
            }
        }
    }

    private fun requestNews() {
        refresh_layout.isRefreshing = true
        OnoyConnector.getNews(this::getNewsCallback)
    }

    private fun getNewsCallback(exception: OnoyException?, newsArray: Array<News>?) {
        refresh_layout.isRefreshing = false
        if (exception != null) {
            Toast.makeText(context, exception.message, Toast.LENGTH_SHORT).show()
        }

        if (newsArray != null) {
            clearAndSetNews(newsArray)
            adapter.notifyDataSetChanged()
        }
    }

    private fun clearAndSetNews(newsArray: Array<News>) {
        news.clear()
        newsArray.reverse()
        news.addAll(newsArray)
    }

    private fun updateFabVisibility() {
        (activity as? MainActivity)?.updateFab(
                FabUpdateEvent(R.drawable.ic_person_add_black_24dp,
                        FabUpdateEvent.Type.Invitation,
                        Runnable { onFabBtnClick() }
                ))
    }

    private fun onFabBtnClick() {
        if (UserManager.isValidCurrentUser())
            Helpers.showInvitationMessage(context!!)
        else
            startActivity(StartFragmentActivity.getIntent(requireContext(), StartFragments.ChooseActionFragment))
    }

    override fun setVisible() {
        isVisibleNow = true
        updateFabVisibility()
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }


}
