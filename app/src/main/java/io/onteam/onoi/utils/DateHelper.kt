package io.onteam.onoi.utils

import java.text.SimpleDateFormat
import java.util.*


object DateHelper {
    private var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

    private fun getDate(): Calendar {
        return Calendar.getInstance()
    }

    fun getCurrentMonth(): Int {
        return getDate().get(Calendar.MONTH) + 1
    }

    fun getCurrentDay(): Int {
        return getDate().get(Calendar.DAY_OF_MONTH)
    }

    fun getCurrentYear(): Int {
        return getDate().get(Calendar.YEAR)
    }

    fun updateDateFormat(pattern: String) {
        simpleDateFormat = SimpleDateFormat(pattern)
    }

    fun getDateWithFormat(pattern: String): String {
        val sdf = SimpleDateFormat(pattern)
        return sdf.format(Date(getDate().timeInMillis))
    }

    fun parseDate(stringDate: String): Date {
        return simpleDateFormat.parse(stringDate)
    }

}