package io.onteam.onoi.objects

import org.simpleframework.xml.*

@Root(strict = false)
class CurrencyRates {
    @get:ElementList(entry = "Currency", inline = true)
    @set:ElementList(entry = "Currency", inline = true)
    var Currency: MutableList<Currency> = mutableListOf()

   /* @get:Attribute(Name = "Name")
    @set:Attribute(Name = "Name")
    var Name: String? = null */

}