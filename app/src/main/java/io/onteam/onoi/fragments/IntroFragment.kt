package io.onteam.onoi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.R
import kotlinx.android.synthetic.main.fragment_intro.view.*

class IntroFragment : Fragment() {

    companion object {
        private const val IMAGE = "IMAGE"
        private const val TITLE = "TITLE"
        private const val MESSAGE = "MESSAGE"

        fun getInstance(imgRes: Int, titleRes: Int, messageRes: Int): IntroFragment {
            val args = Bundle()
            args.putInt(IMAGE, imgRes)
            args.putInt(TITLE, titleRes)
            args.putInt(MESSAGE, messageRes)
            val introFragment = IntroFragment()
            introFragment.arguments = args
            return introFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_intro, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        var imageRes = R.drawable.spalsh_logo
        var titleRes = R.string.empty
        var messageRes = R.string.empty

        arguments?.let {
            imageRes = it.getInt(IMAGE)
            titleRes = it.getInt(TITLE)
            messageRes = it.getInt(MESSAGE)
        }

        with(view) {
            iv_image.setImageResource(imageRes)
            tv_title.setText(titleRes)
            tv_message.setText(messageRes)
        }
    }


}
