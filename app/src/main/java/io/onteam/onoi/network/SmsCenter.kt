package io.onteam.onoi.network

import okhttp3.*
import java.io.IOException
import java.lang.StringBuilder

class SmsCenter {
    //http://217.29.21.2/smssend/?to=996501190091&secret=17033905-6e2b-47dc-b0dd-3c0cf6a786fa&text=1234
    private val url = "http://217.29.21.2/smssend/"
    private val token = "17033905-6e2b-47dc-b0dd-3c0cf6a786fa"

    fun sendSms(phone: String, text: String, callback: (Boolean) -> Unit) {
        val okHttpClient = OkHttpClient()
        val request = Request.Builder().url(buildUrl(phone, text)).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call?, e: IOException?) {
                callback(false)
            }

            override fun onResponse(call: Call?, response: Response?) {
                val status = response?.body()?.string()
                val isSuccess = status == "OK"
                callback(isSuccess)
            }
        })
    }

    private fun buildUrl(phone: String, text: String): String {
        val correctedPhone = phone.replace("+", "").replace(" ", "")
        return StringBuilder(url)
                .append("?")
                .append("secret=$token")
                .append("&text=$text")
                .append("&to=$correctedPhone")
                .toString()
    }
}