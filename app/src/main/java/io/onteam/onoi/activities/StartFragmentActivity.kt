package io.onteam.onoi.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.KeyEvent
import io.onteam.onoi.base.BaseFragmentActivity
import io.onteam.onoi.fragments.FinishFragment
import io.onteam.onoi.fragments.newdesign.ChooseActionFragment
import io.onteam.onoi.fragments.newdesign.LangChooseFragment
import io.onteam.onoi.objects.StartFragments
import io.onteam.onoi.utils.NotificationHelper

class StartFragmentActivity : BaseFragmentActivity() {

    companion object {
        private const val START_FRAGMENT_ORDINAL = "START_FRAGMENT_ORDINAL"
        private const val NOTIFICATION_ID = "NOTIFICATION_ID"

        fun getIntent(context: Context, startFragment: StartFragments = StartFragments.LangChooseFragment, notificationId: Int = -1): Intent {
            val intent = Intent(context, StartFragmentActivity::class.java)
            intent.putExtra(START_FRAGMENT_ORDINAL, startFragment.ordinal)
            intent.putExtra(NOTIFICATION_ID, notificationId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = intent.getIntExtra(NOTIFICATION_ID, -1)
        if (id > 0)
            NotificationHelper.clearNotification(id)
    }

    override fun getFragment(): Fragment {
        return getFragmentByOrdinal(intent.getIntExtra(START_FRAGMENT_ORDINAL, 0))
    }

    private fun getFragmentByOrdinal(ordinal: Int): Fragment {
        return when (ordinal) {
            0 -> LangChooseFragment()
            1 -> ChooseActionFragment()
            else -> LangChooseFragment()
        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val fragment = getCurrentFragment()

            if (fragment is FinishFragment)
                return true

            return if (supportFragmentManager.backStackEntryCount > 0) {
                popFragment()
                true
            } else {
                finish()
                true
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}
