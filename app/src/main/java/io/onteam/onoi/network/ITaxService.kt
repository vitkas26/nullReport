package io.onteam.onoi.network

import io.onteam.onoi.objects.RegisteredUser
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Описывает функции доступные для использования в системе налоговой.
 */
interface ITaxService {
    companion object {
        //Ссылка на систему налоговой
        const val URL = "http://infocom.onoi.kg/"
    }

    /**
     * Возвращает информацию по пользователю из БД налоговой.
     * @param inn - ИНН пользвателя.
     */
    @GET("info/{inn}")
    fun getUserInfo(@Path("inn") inn: String): Call<RegisteredUser>
}