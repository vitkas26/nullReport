package io.onteam.onoi.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.R
import io.onteam.onoi.objects.ChatMessage
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.item_message_post.view.*

class MessageAdapter(var messages: MutableList<ChatMessage>) : RecyclerView.Adapter<MessageAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val layoutRes = R.layout.item_message_post
        val view = LayoutInflater
                .from(parent.context).inflate(layoutRes, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindView(messages[position])
    }

    inner class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        fun bindView(messagePost: ChatMessage) {
            if (messagePost.is_user)
                constructOutgoingView(messagePost)
            else
                constructIncommingView(messagePost)
        }

        private fun constructOutgoingView(messagePost: ChatMessage) {
            itemView.rl_incomming_view.setVisible(false)
            itemView.rl_outgoing_view.setVisible(true)
            itemView.tv_message_out.text = messagePost.message
        }

        private fun constructIncommingView(messagePost: ChatMessage) {
            itemView.rl_outgoing_view.setVisible(false)
            itemView.rl_incomming_view.setVisible(true)
            itemView.tv_message_in.text = messagePost.message
        }
    }
}