package io.onteam.onoi.objects

class Report {
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var destination_type: String = ""
    var period_type: String = ""
    var months: Array<Int> = emptyArray()
    var period_start: String = ""
    var period_end: String = ""
    var last_day: Int = 0
    var number: String = ""
}