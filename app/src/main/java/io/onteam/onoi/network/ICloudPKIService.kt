package io.onteam.onoi.network

import io.onteam.onoi.objects.AuthCheckResponse
import io.onteam.onoi.objects.AuthEmployeeCheckResponse
import io.onteam.onoi.objects.EmployeePinConfirmResponse
import io.onteam.onoi.objects.PinConfirmResponse
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Описывает функции доступные для использования в инфраструктуре CloudPKI.
 */
interface ICloudPKIService {
    companion object {
        //Ссылка на систему CloudPKI
        const val URL = "https://sign.onoi.kg/api/"
    }

    @POST("auth/check")
    fun checkAuth(@Body data: RequestBody): Call<AuthCheckResponse>

    @POST("auth/employee/check")
    fun employeeCheck(@Body body: RequestBody): Call<AuthEmployeeCheckResponse>

    @POST("auth/employee/pin/confirm")
    fun pinEmployeeConfirm(@Body body: RequestBody): Call<EmployeePinConfirmResponse>

    @POST("auth/pin/confirm")
    fun pinConfirm(@Body body: RequestBody): Call<PinConfirmResponse>
}