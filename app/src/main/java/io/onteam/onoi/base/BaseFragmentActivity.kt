package io.onteam.onoi.base

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dmax.dialog.SpotsDialog
import io.onteam.onoi.R

abstract class BaseFragmentActivity : AppCompatActivity(), IBaseFragmentActivity {

    lateinit var progressDialog: SpotsDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        progressDialog = SpotsDialog(this, R.style.CustomSpotsDialog)

        replaceFragment(getFragment(), withAnim = false)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        replaceFragment(getFragment())
    }

    override fun replaceFragment(fragment: Fragment, isUseBackstack: Boolean, withAnim: Boolean) {
        val transaction = supportFragmentManager
                .beginTransaction()

        if (isUseBackstack) {
            val name = fragment::class.java.name + "_" + supportFragmentManager.backStackEntryCount
            transaction.addToBackStack(name)
        } else {
            clearBackStack()
        }

        if (withAnim)
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                    android.R.anim.slide_in_left, android.R.anim.slide_out_right)

        transaction
                .replace(R.id.fl_base, fragment)
                .commit()
    }

    private fun clearBackStack() {
        for (i: Int in 0..supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun popFragment() {
        supportFragmentManager.popBackStack()
    }

    abstract fun getFragment(): Fragment

    fun getCurrentFragment(): Fragment? {
        return supportFragmentManager?.findFragmentById(R.id.fl_base)
    }
}

interface IBaseFragmentActivity {
    fun replaceFragment(fragment: Fragment, isUseBackstack: Boolean = false, withAnim: Boolean = true)
    fun popFragment()
}
