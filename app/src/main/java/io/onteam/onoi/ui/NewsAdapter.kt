package io.onteam.onoi.ui

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import io.onteam.onoi.R
import io.onteam.onoi.objects.News
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.news_adapter.view.*

class NewsAdapter(news: List<News>) : RecyclerView.Adapter<NewsAdapter.Holder>() {


    private val news: List<News> = news
    private var lastShownNewsHolder: Holder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_adapter, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindView(news[position])
    }

    override fun getItemCount(): Int {
        return news.size
    }

    inner class Holder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindView(news: News) {

            val imageView = view.findViewById<ImageView>(R.id.iv_news_img)
            if (news.main_img != null) {
                imageView.setVisible(true)
                Glide.with(view.context)
                        .load(Uri.parse(news.main_img))
                        .into(imageView)
            } else {
                imageView.setVisible(false)
            }

            //Короткое описание
            view.tv_date.text = news.date
            view.tv_news_provider.text = news.source
            view.tv_news_header.text = news.title
            view.tv_news_short.text = news.content
            view.btn_show_news.setOnClickListener({
                showFullNews()
            })
            view.btn_hide_news.setOnClickListener({
                setNewsState(false)
            })

            //Полное описание
            view.tv_news_large.text = news.content
            view.btn_share_news.setOnClickListener { share(news) }

            view.expl_short_news.setOnClickListener {
                showFullNews()
            }

        }

        private fun share(news: News) {
            val shareText = "${news.title} ${news.source_url}"
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, shareText)
            intent.type = "text/plain"
            view.context.startActivity(Intent.createChooser(intent, view.context.getString(R.string.select_application)))
        }

        private fun setNewsState(needShowLarger: Boolean) {

            val rotationValue = if (needShowLarger) 90f else 0f
            view.iv_news_arrow.animate().rotation(rotationValue).start()

            view.expl_short_news.toggle()
            view.expl_full_news.toggle()
            lastShownNewsHolder = if (needShowLarger) this else null
        }

        private fun showFullNews() {
            lastShownNewsHolder?.setNewsState(false)
            setNewsState(true)
        }

    }
}