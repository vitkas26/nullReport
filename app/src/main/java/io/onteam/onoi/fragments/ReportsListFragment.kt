package io.onteam.onoi.fragments


import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dmax.dialog.SpotsDialog
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.activities.StartFragmentActivity
import io.onteam.onoi.objects.*
import io.onteam.onoi.ui.ReportsAdapter
import io.onteam.onoi.utils.DateHelper
import kotlinx.android.synthetic.main.fragment_reports_list.*
import kotlinx.android.synthetic.main.fragment_reports_list.view.*


/**
 *  Fragment используемый для отображения списка отчетов пользователя.
 */
class ReportsListFragment : BaseReportFragment(), FragmentVisibleListener {

    private val layoutRes = R.layout.fragment_reports_list

    //Отчеты пользователя.
    private val reports = mutableListOf<ReportGroupedItem>()
    //Адаптер для списка отчетов.
    private val adapter = ReportsAdapter(reports, null)

    private var progress: Dialog? = null
    private var errorCounter: Int = 0
    private var isVisibleNow = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        initViews(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress = SpotsDialog(context, R.style.CustomSpotsDialog)
        requestInfo()
    }


    private fun sendAllReports() {
        val inn = UserManager.getCurrentUser()?.inn ?: ""

        val saveAllReportsCall = OnoyConnector.saveAllReports(inn) { onoyException, isSuccess ->
            progress?.dismiss()
            onoyException?.apply {
                showAlertDialog(message)
            }

            if (isSuccess) {
                requestInfo(Runnable { notifyReportListChangeListener() })
                Toast.makeText(context, R.string.all_reports_sending_success, Toast.LENGTH_SHORT).show()
            }
        }

        progress?.setOnCancelListener { saveAllReportsCall.cancel() }
        progress?.show()
    }

    private fun showAlertDialog(message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    /**
     * Запращивает список отчетов.
     */
    private fun requestInfo(callback: Runnable? = null) {
        refresh_layout.isRefreshing = true
        if (UserManager.isValidCurrentUser()) {
            val user = UserManager.getCurrentUser()!!
            requestSingleUserInfo(user, callback)
        } else {
            requestFakeUserReports()
        }
    }

    private fun requestFakeUserReports() {
        OnoyConnector.getFakeReports { onoyException, arrayOfReportGroups ->
            refresh_layout.isRefreshing = false
            onoyException?.apply {
                Toast.makeText(context, "Отчеты: $message", Toast.LENGTH_SHORT).show()
                if (isVisibleNow)
                    updateFabVisibility()
            }


            arrayOfReportGroups?.let {
                reports.clear()
                reports.add(ReportGroupedItem("Ваша компания", it))
                adapter.notifyDataSetChanged()
                if (isVisibleNow)
                    updateFabVisibility()
            }
        }
    }

    private fun requestSingleUserInfo(user: UserProfile, callback: Runnable?) {
        OnoyConnector.getAbonentReports(user.inn) { onoyException, arrayOfReportGroups ->
            refresh_layout.isRefreshing = false

            resetEmptyLayout()

            onoyException?.apply {
                Toast.makeText(context, "Отчеты: $message", Toast.LENGTH_SHORT).show()
                if (isNetworkException) {
                    reports.clear()
                    adapter.notifyDataSetChanged()
                    setEmptyPreload(EmptyPreload.getNoNetwork())
                }
                if (isVisibleNow)
                    updateFabVisibility()
                callback?.run()
            }

            arrayOfReportGroups?.let { it ->

                val filterReportsGroup = mutableListOf<ReportGroup>()

                it.forEach {
                    val filteredReports = filterReports(it.reports)
                    if (filteredReports.isNotEmpty())
                        filterReportsGroup.add(ReportGroup(it.name, filteredReports))
                }

                filterReportsGroup.forEach {
                    reports.add(ReportGroupedItem(user.name, filterReportsGroup.toTypedArray()))
                }

                adapter.notifyDataSetChanged()

                if (reports.isEmpty())
                    setEmptyPreload(EmptyPreload.getSended())

                if (isVisibleNow)
                    updateFabVisibility()
                callback?.run()
            }
        }
    }

    private fun setEmptyPreload(preload: EmptyListPreload) {
        el_message.setPreload(preload)
    }

    private fun resetEmptyLayout() {
        el_message.resetPreload()
    }

    override fun setVisible() {
        isVisibleNow = true
        updateFabVisibility()
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }

    private fun updateFabVisibility() {
        val runnable = if (reports.isEmpty()) {
            null
        } else {
            getFabClickRunnable()
        }
        (activity as? MainActivity)?.updateFab(
                FabUpdateEvent(R.drawable.ic_send_black_24dp, FabUpdateEvent.Type.SendReport, runnable))
    }

    private fun getFabClickRunnable(): Runnable {
        return Runnable {
            if (UserManager.isValidCurrentUser())
                showSendConfirmationDialog()
            else
                showStartActivity()
        }
    }

    private fun showStartActivity() {
        startActivity(StartFragmentActivity.getIntent(requireContext(), StartFragments.ChooseActionFragment))
    }

    private fun showSendConfirmationDialog() {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setTitle(R.string.confirm_action)
                .setMessage(R.string.send_report_confirmation_message)
                .setPositiveButton(R.string.send) { _, _ ->
                    sendAllReports()
                }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }

    /**
     * Сбрасывает счетчик неуспешных запросов.
     */
    private fun resetErrorCounter() {
        errorCounter = 0
    }

    /**
     * Фильтруем отчеты пользователя по месяцам. Если сейчас месяц который содердится в массиве {@code months}
     * в отчете.
     * @param  originalReports отчеты пользователя.
     */
    private fun filterReports(originalReports: Array<Report>): Array<Report> {
        val currentMonth = DateHelper.getCurrentMonth()
        return originalReports.filter {
            it.months.contains(currentMonth)
        }.toTypedArray()

    }

    private fun notifyReportListChangeListener() {
        changeListener?.reloadData()
    }

    /**
     * Инициализация компонентов fragment'a.
     * @param view родительский View.
     */
    private fun initViews(view: View) {
        view.refresh_layout.setColorSchemeResources(R.color.colorAccent)
        view.refresh_layout.setOnRefreshListener {
            resetErrorCounter()
            requestInfo()
        }

        view.rv_reports.adapter = adapter
        view.rv_reports.layoutManager = LinearLayoutManager(context)
    }
}
