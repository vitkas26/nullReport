package io.onteam.onoi.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Toast
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.fragments.*
import io.onteam.onoi.objects.FabUpdateEvent
import io.onteam.onoi.objects.StartFragments
import io.onteam.onoi.objects.UserManager
import io.onteam.onoi.ui.BottomNavigationPresenter
import io.onteam.onoi.utils.isVisible
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Основное Activity приложения.
 */
class MainActivity : AppCompatActivity(), AHBottomNavigation.OnTabSelectedListener {

    private val layoutRes = R.layout.activity_main
    private lateinit var navigationPresenter: BottomNavigationPresenter
    private var lastDrawableResId: Int? = null
    private val closeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            finish()
        }
    }

    companion object {
        const val FINISH_ACTION = "FINISH_ACTION"

        fun getIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        navigationPresenter = BottomNavigationPresenter(bottom_navigation, this, this)

        val viewPageAdapter = getViewPageAdapter()
        vp_main.adapter = viewPageAdapter
        vp_main.offscreenPageLimit = viewPageAdapter.count

        navigationPresenter.setSelectedItem(0, true)

        registerCloseReceiver()
    }

    override fun onDestroy() {
        unregisterCloseReceiver()
        super.onDestroy()
    }

    private fun registerCloseReceiver() {
        val filter = IntentFilter(FINISH_ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(closeReceiver, filter)
    }

    private fun unregisterCloseReceiver() {
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(closeReceiver)
        } catch (ignored: Exception) {
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            handleBackClick()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private var lastClickMills: Long = 0

    private fun handleBackClick() {
        val currentItem = (vp_main.adapter as MainViewPageAdapter).getItem(vp_main.currentItem)
        if (currentItem is WikiFragment && currentItem.canGoBack()) {
            currentItem.goBack()
            return
        }

        if (System.currentTimeMillis() - lastClickMills < 1000) {
            finish()
        } else {
            lastClickMills = System.currentTimeMillis()
            Toast.makeText(this, R.string.click_for_exit_message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getViewPageAdapter(): MainViewPageAdapter {
        val adapter = MainViewPageAdapter(supportFragmentManager)
        adapter.addFragment(MainFragment())
        adapter.addFragment(ReportsParentFragment())
        adapter.addFragment(WikiFragment())
        adapter.addFragment(ChatFragment())
        if (UserManager.getCurrentUser()?.tax_agent == true) {
            adapter.addFragment(TaxAgentProfileFragment())
        } else {
            adapter.addFragment(ProfileFragment())
        }
        return adapter
    }

    fun updateFab(updateEvent: FabUpdateEvent?) {
        if (updateEvent != null) {
            fab_main.setOnClickListener { updateEvent.action?.run() }
            if (updateEvent.action == null)
                animateFabState(null, false)
            else
                animateFabState(updateEvent.drawRes, true)
        } else {
            animateFabState(null, false)
        }
    }

    private fun animateFabState(drawableRes: Int?, isShow: Boolean) {
        if (!fab_main.isVisible() && !isShow) return
        if (lastDrawableResId == drawableRes) return

        val drawable = getFabDrawableByResId(drawableRes)

        fab_main.clearAnimation()
        if (isShow) {
            fab_main.setImageDrawable(drawable)
        }

        val from = if (isShow) 0f else 1f
        val to = if (isShow) 1f else 0f

        val scaleAnimation = ScaleAnimation(from, to, from, to, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.duration = 50//if (isShow) 100 else 1
        scaleAnimation.interpolator = AccelerateInterpolator()
        scaleAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                fab_main.visibility = if (isShow) View.VISIBLE else View.INVISIBLE
            }

            override fun onAnimationStart(animation: Animation?) {
            }

        })
        fab_main.startAnimation(scaleAnimation)
    }

    private fun getFabDrawableByResId(drawableRes: Int?): Drawable? {
        lastDrawableResId = drawableRes
        return if (drawableRes != null)
            ContextCompat.getDrawable(this, drawableRes)
        else null
    }


    override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
        val lastItem = (vp_main.adapter as MainViewPageAdapter).getItem(vp_main.currentItem)
        if (lastItem is FragmentVisibleListener)
            (lastItem as FragmentVisibleListener).setUnvisible()

        if (!UserManager.isValidCurrentUser() && position == 4) {
            startActivity(StartFragmentActivity.getIntent(this, StartFragments.ChooseActionFragment))
            return false
        }

        vp_main.currentItem = position
        val item = (vp_main.adapter as MainViewPageAdapter).getItem(position)
        if (item is FragmentVisibleListener) (item as FragmentVisibleListener).setVisible()
        return true
    }

    inner class MainViewPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        private val fragments = mutableListOf<Fragment>()

        fun addFragment(fragment: Fragment) {
            fragments.add(fragment)
        }

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return fragments.size
        }


    }
}
