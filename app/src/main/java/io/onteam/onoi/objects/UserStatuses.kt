package io.onteam.onoi.objects

object UserStatuses {
    const val ok = 0
    const val notFound = 1
    const val searchError = 100
}