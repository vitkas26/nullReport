package io.onteam.onoi.objects

import io.onteam.onoi.utils.empty

class PinConfirmResponse : BaseResponse() {
    var approve: Boolean = false
    var inn: String = String.empty()
    var token: String = String.empty()
}