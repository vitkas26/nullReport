package io.onteam.onoi.objects

data class PinConfirm(val pin: String, val innOrganisation: String, val innEmployee: String?)