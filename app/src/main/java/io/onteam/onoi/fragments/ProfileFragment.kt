package io.onteam.onoi.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import dmax.dialog.SpotsDialog
import io.onteam.onoi.FragmentVisibleListener
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import io.onteam.onoi.activities.PhotoViewActivity
import io.onteam.onoi.objects.*
import io.onteam.onoi.utils.Helpers
import io.onteam.onoi.utils.Preferences
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment(), FragmentVisibleListener {

    private val layoutRes = R.layout.fragment_profile
    private var isVisibleNow = false
    private var balanceChangeReceiver: BroadcastReceiver? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerBalanceReceiver()
        setDataInUi()
    }

    override fun onDestroyView() {
        unregisterReceiver()
        super.onDestroyView()
    }

    private fun unregisterReceiver() {
        val broadcastManager = LocalBroadcastManager.getInstance(requireContext())
        try {
            balanceChangeReceiver?.let {
                broadcastManager.unregisterReceiver(it)
            }
        } catch (ignored: Exception) {
        }
    }

    private fun registerBalanceReceiver() {
        balanceChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let {
                    val double = it.getDoubleExtra("balance", 0.0)
                    updateBalance(double)
                }
            }
        }

        LocalBroadcastManager.getInstance(requireContext())
                .registerReceiver(balanceChangeReceiver!!,
                        IntentFilter(LocalBroadcatActions.CHANGE_BALANCE))
    }

    private fun updateBalance(balance: Double) {
        civ_balance?.apply {
            setText(context.getString(R.string.your_balance_format, balance))
        }
    }

    private fun setDataInUi() {
        val user = UserManager.getCurrentUser()
        if (user != null) {
            tv_company_name.text = user.name
            civ_director.setText(user.chief)
            civ_inn.setText(user.inn)
            updateBalance(user.balance)

            cb_auto_sent.isChecked = user.auto_sent
            cb_auto_sent.setOnCheckedChangeListener(getAutosentChangeListener(user.inn))

            rl_exit.setOnClickListener { removeUser() }
        }
    }

    private fun removeUser() {
        AlertDialog.Builder(requireContext(), R.style.ExitAlertDialogTheme)
                .setTitle(R.string.confirm_action)
                .setMessage(R.string.exit_query_message)
                .setPositiveButton(R.string.cancel, null)
                .setNegativeButton(R.string.exit) { _, _ ->
                    run {
                        UserManager.resetCurrentUser()

                        Preferences.setValue(Preferences.Keys.USER_KEYWORD, "")
                        Preferences.setValue(Preferences.Keys.USER_INN, "")

                        requireActivity().finish()
                    }
                }
                .create().show()
    }

    private fun getAutosentChangeListener(userInn: String): (CompoundButton, Boolean) -> Unit = { _, isChecked -> setAutoSent(userInn, isChecked) }

    private fun setAutoSent(userInn: String, checked: Boolean) {
        val pd = SpotsDialog(context!!, R.style.CustomSpotsDialog)
        val autosentCall = OnoyConnector.setAutosentState(userInn, checked) { exception, success ->
            pd.dismiss()
            if (exception != null) {
                showAlertMessage(exception.message)
            }
            val newCheckedValue = if (success) checked else !checked
            setAutosentStateWithoutCallback(newCheckedValue, userInn)
        }
        pd.setOnCancelListener { autosentCall.cancel() }
        pd.show()
    }

    private fun setAutosentStateWithoutCallback(checked: Boolean, userInn: String) {
        cb_auto_sent.setOnCheckedChangeListener(null)
        cb_auto_sent.isChecked = checked
        cb_auto_sent.setOnCheckedChangeListener(getAutosentChangeListener(userInn))
    }

    private fun showAlertMessage(message: String) {
        AlertDialog.Builder(context!!, R.style.CustomAlertDialogTheme)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .create().show()
    }

    override fun setVisible() {
        isVisibleNow = true
        (activity as? MainActivity)?.updateFab(FabUpdateEvent(R.drawable.ic_person_add_black_24dp,
                FabUpdateEvent.Type.Invitation,
                Runnable { Helpers.showInvitationMessage(context!!) }))
    }

    override fun setUnvisible() {
        isVisibleNow = false
    }
}
