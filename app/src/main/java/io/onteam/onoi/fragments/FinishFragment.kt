package io.onteam.onoi.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.onteam.onoi.R
import io.onteam.onoi.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_finish.view.*

class FinishFragment : Fragment() {

    private val layoutRes = R.layout.fragment_finish

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes, container, false)
        init(view)
        return view
    }

    private fun init(view: View) {
        LocalBroadcastManager.getInstance(requireContext())
                .sendBroadcast(Intent(MainActivity.FINISH_ACTION))

        view.btn_next.setOnClickListener{
            startActivity(MainActivity.getIntent(requireContext()))
            requireActivity().finish()
        }
    }

}
