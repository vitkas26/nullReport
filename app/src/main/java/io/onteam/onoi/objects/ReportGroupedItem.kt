package io.onteam.onoi.objects

data class ArchiveGroupedItem(var company: String, var reportsGroup: Array<ArchiveGroup>)
data class ReportGroupedItem(var company: String, var reportGroup: Array<ReportGroup>)