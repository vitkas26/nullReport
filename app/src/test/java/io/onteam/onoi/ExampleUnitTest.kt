package io.onteam.onoi

import io.onteam.onoi.network.OnoiService
import io.onteam.onoi.utils.DateHelper
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }


    @Test
    fun getMessageTest(){
        val onoiService = OnoiService().getOnoiService()
        val execute = onoiService.getTop100Messages(32).execute()
        Assert.assertNotNull(execute)
        Assert.assertTrue(execute.isSuccessful)
        val body = execute.body()
        Assert.assertNotNull(body)
        Assert.assertTrue(body!!.isNotEmpty())
    }

    @Test
    fun testDate(){
        val year = 2018
        val month = 4
        val day = 27
        val parseDate = DateHelper.parseDate("$year-$month-$day 22:15:25")
        Assert.assertNotNull(parseDate)
        val calendar = Calendar.getInstance()
        calendar.time = parseDate

        Assert.assertEquals(year, calendar.get(Calendar.YEAR))
        Assert.assertEquals(month - 1, calendar.get(Calendar.MONTH))
        Assert.assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH))
    }
}
