package io.onteam.onoi.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import io.onteam.onoi.R
import kotlinx.android.synthetic.main.month_picker_dialog.view.*
import java.util.*

class SimpleMonthPickerDialog : AppCompatDialogFragment() {

    interface OnSaveListener{
        fun save(string: String)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val contentView = LayoutInflater.from(context!!).inflate(R.layout.month_picker_dialog, null, false)

        val calendar = Calendar.getInstance()

        var monthVal = calendar.get(Calendar.MONTH) + 1
        var yearVal = calendar.get(Calendar.YEAR)

        if(arguments != null){
            val split = arguments?.getString("date")?.split(".")
            if(split != null) {
                monthVal = split[0].toInt()
                yearVal = split[1].toInt()
            }
        }

        contentView.np_from_month.minValue = 1
        contentView.np_from_month.maxValue = 12
        contentView.np_from_month.value = monthVal

        contentView.np_from_year.minValue = 2000
        contentView.np_from_year.maxValue = calendar.get(Calendar.YEAR)
        contentView.np_from_year.value = yearVal

        return AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
                .setView(contentView)
                .setTitle(R.string.select_range_title)
                .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                    val date = String.format("%02d.%d", contentView.np_from_month.value,
                            contentView.np_from_year.value)
                    onSaveListener?.save(date)
                }
                .setNegativeButton(R.string.cancel, null)
                .create()
    }

    private var onSaveListener: OnSaveListener? = null

    fun setSaveListiner(onSaveListener: OnSaveListener){
        this.onSaveListener = onSaveListener
    }
}