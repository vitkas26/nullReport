package io.onteam.onoi.objects

class ArchivedReportResponse {
    var success: Boolean = false
    var message: String = ""
    var forms: Array<ArchiveGroup> = emptyArray()
}