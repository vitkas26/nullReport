package io.onteam.onoi.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import io.onteam.onoi.R
import io.onteam.onoi.objects.EmptyListPreload
import io.onteam.onoi.utils.setVisible
import kotlinx.android.synthetic.main.view_empty.view.*

class EmptyLayout(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : RelativeLayout(context, attrs, defStyleAttr) {

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?) : this(context, null, 0)

    init {

        val typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.EmptyLayout_Base, 0, 0)

        val title = typedArray.getResourceId(R.styleable.EmptyLayout_Base_titleText, 0)
        val message = typedArray.getResourceId(R.styleable.EmptyLayout_Base_messageText, 0)
        val img = typedArray.getResourceId(R.styleable.EmptyLayout_Base_img, 0)

        typedArray.recycle()

        val inflater = getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.view_empty, this, true)

        if (title > 0) {
            view.tv_title.setText(title)
            view.tv_title.setVisible(true)
        } else
            view.tv_title.setVisible(false)

        if (message > 0) {
            view.tv_message.setText(message)
            view.tv_message.setVisible(true)
        } else
            view.tv_message.setVisible(false)

        if (img > 0) {
            view.iv_image.setImageResource(img)
            view.iv_image.setVisible(true)
        } else
            view.iv_image.setVisible(false)

        refreshDrawableState()
    }

    fun setPreload(preload: EmptyListPreload) {
        if (preload.title > 0) {
            tv_title.setText(preload.title)
            tv_title.setVisible(true)
        } else
            tv_title.setVisible(false)

        if (preload.message > 0) {
            tv_message.setText(preload.message)
            tv_message.setVisible(true)
        } else
            tv_message.setVisible(false)

        if (preload.image > 0) {
            iv_image.setImageResource(preload.image)
            iv_image.setVisible(true)
        } else
            iv_image.setVisible(false)
    }

    fun resetPreload() {
        tv_title.setVisible(false)
        tv_message.setVisible(false)
        iv_image.setVisible(false)
    }
}