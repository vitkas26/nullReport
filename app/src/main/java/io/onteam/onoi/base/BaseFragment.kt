package io.onteam.onoi.base


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import java.lang.RuntimeException

abstract class BaseFragment : Fragment() {

    private lateinit var mActivity: IBaseFragmentActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setSoftInputMode(getWindowSoftInputMode())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutRes(), container, false)
    }

    open fun getWindowSoftInputMode(): Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN

    abstract fun getLayoutRes(): Int

    fun replaceFragment(fragment: Fragment, isUseBackstack: Boolean = false) {
        mActivity.replaceFragment(fragment, isUseBackstack)
    }

    fun popFragment() {
        mActivity.popFragment()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is IBaseFragmentActivity) {
            mActivity = context
        } else {
            throw RuntimeException(context.toString() + " must implement IBaseFragmentActivity")
        }
    }
}
