package io.onteam.onoi.fragments.newdesign


import android.os.Bundle
import android.view.View
import io.onteam.onoi.R
import io.onteam.onoi.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_step.*


class ChooseActionFragment : BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_select_step

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(view) {
            btn_signin.setOnClickListener { onSignInBtnClick() }
            btn_registration.setOnClickListener { onRegistrationBtnClick() }
        }
    }

    private fun onRegistrationBtnClick() {
        replaceFragment(SignInFragment(), true)
        //replaceFragment(UserTypeFragment(), true)
    }

    private fun onSignInBtnClick() {
        replaceFragment(SignInFragment(), true)
    }
}
