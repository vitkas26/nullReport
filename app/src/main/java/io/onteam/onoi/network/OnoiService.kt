package io.onteam.onoi.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Класс для работы с системами Оной и налоговой с использованием Retrofit.
 */
class OnoiService {

    //Интерфейс для работы с Оной.
    private var mOnoiService: IOnoiService? = null

    //Интерфейс для работы БД налоговой.
    private var mTaxService: ITaxService? = null

    //Интерфейс для работы CloudPKI.
    private var mCloudPKIService: ICloudPKIService? = null


    private val timeOut: Long = 10

    private val client = OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                var count = 0
                val maxAttempts = 3

                val request = chain.request()
                var response = chain.proceed(request)

                while (response?.isSuccessful != true && count <= maxAttempts) {
                    count++
                    response = chain.proceed(request)
                }
                response!!
            }
            .build()

    //Объект Retrofit для работы с системой Оной.
    private val mOnoiRetrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(IOnoiService.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    //Объект Retrofit для работы с системой БД налоговой.
    private val mTaxRetrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(ITaxService.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    //Объект Retrofit для работы с CloudPKI.
    private val mCloudPKIRetrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(ICloudPKIService.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    /**
     * Возвращает интерфейс для использования методов системы Оной.
     */
    fun getOnoiService(): IOnoiService {
        if (mOnoiService == null)
            mOnoiService = mOnoiRetrofit.create(IOnoiService::class.java)
        return mOnoiService!!
    }

    /**
     * Возвращает интерфейс для использования методов БД налогой.
     */
    fun getTaxService(): ITaxService {
        if (mTaxService == null)
            mTaxService = mTaxRetrofit.create(ITaxService::class.java)
        return mTaxService!!
    }


    /**
     * Возвращает интерфейс для использования методов CloudPKI.
     */
    fun getCloudPKIService(): ICloudPKIService {
        if (mCloudPKIService == null)
            mCloudPKIService = mCloudPKIRetrofit.create(ICloudPKIService::class.java)
        return mCloudPKIService!!
    }
}