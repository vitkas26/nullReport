package io.onteam.onoi.objects

data class ReportGroup(val name: String, val reports: Array<Report> = emptyArray())