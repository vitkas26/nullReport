package io.onteam.onoi

import android.os.Handler
import android.os.Looper
import io.onteam.onoi.objects.ChatMessage
import io.onteam.onoi.objects.OnoyConnector
import io.onteam.onoi.ui.MessageAdapter
import io.onteam.onoi.utils.NotificationHelper

object MessageManager {

    private val messagesKey = "messages"

    private val messages: MutableList<ChatMessage> = mutableListOf()
    var messageAdapter: MessageAdapter = MessageAdapter(messages)
    private val handler = Handler(Looper.getMainLooper())

    fun addMessageWithNotification(messagePost: ChatMessage) {
        handler.post {
            messages.add(0, messagePost)
            messageAdapter.notifyItemInserted(0)
            if (!messagePost.is_user)
                showNotification(messagePost.message)
        }
    }

    private fun showNotification(message: String) {
        if (isAdapterHasObservers())
            NotificationHelper.playNotificationRingtone()
        else
            NotificationHelper.showChatMessageNotifiction(message)
    }

    private fun isAdapterHasObservers(): Boolean {
        return messageAdapter.hasObservers()
    }


    private fun addAllMessages(messages: Array<ChatMessage>) {
        this.messages.clear()
        this.messages.addAll(messages)
        handler.post { messageAdapter.notifyDataSetChanged() }
    }


    fun requestTop100Message(id: Int) {
        OnoyConnector.getTop100Messages(id) { chatMessages ->
            chatMessages.reverse()
            addAllMessages(chatMessages)
        }
    }
}